#include "pch.h"
#include "CppUnitTest.h"
#include "../Lost Cities Duel/Deck.h"
#include "../Lost Cities Duel/Player.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace LostCitiesDuelTest
{
	TEST_CLASS(DeckTests)
	{
	public:

		TEST_METHOD(GetNumberOfCards)
		{
			Deck d;
			uint16_t howmanycards = d.GetNumberOfCards();
			uint16_t maximnumberofcards = 60;
			Assert::IsTrue(howmanycards==maximnumberofcards ,L"The deck is not complete!");
		}

	};
}
