#include "pch.h"
#include "CppUnitTest.h"

#include"../Lost Cities Duel/Deck.h"
#include "../Lost Cities Duel/Player.h"
#include "../Lost Cities Duel/Table.h"

#include<sstream>
#include<string>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace LostCitiesDuelTest
{

	TEST_CLASS(TableTests)
	{
		TEST_METHOD(LineMinusOneColumnOne)
		{
			Table table;
			Assert::ExpectException<std::out_of_range>([table]() {
				table[{-1, 1}];
				});
		}
		TEST_METHOD(LineMinusOneColumnOneConst)
		{
			const Table table;
			Assert::ExpectException<std::out_of_range>([&]() {
				table[{-1, 1}];
				});
		}
		TEST_METHOD(BoardColumnMinusOneColumn)
		{
			Table table;
			Assert::ExpectException<std::out_of_range>([table]() {
				table[-1];
				});

		}
		TEST_METHOD(BoardColumnMinusOneConst)
		{
			const Table table;
			Assert::ExpectException<std::out_of_range>([&]() {
				table[-1];
				});
		}
	};
}
