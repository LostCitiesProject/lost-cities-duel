#include "pch.h"
#include "CppUnitTest.h"
#include"../Lost Cities Duel/Deck.h"
#include "../Lost Cities Duel/Player.h"
#include"../Lost Cities Duel/LostCitiesDuelGame.h"
#include<sstream>
#include<string>
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace LostCitiesDuelTest
{
	TEST_CLASS(PlayerTest)
	{
	public:

		TEST_METHOD(Constructor)
		{
			Player player(15, "Marian");
			Assert::IsTrue(player.GetAge() == 15);
			Assert::AreEqual(player.GetName(), std::string("Marian"), L"Player is not the same!");
		}
		TEST_METHOD(Print)
		{
			Player player(15, "Marian");
			std::stringstream stream;
			stream << player;
			Assert::AreEqual(std::string("Marian"), stream.str(), L"Player is not the same!");
		}

	
			TEST_METHOD(PickCard)
			{
				Deck d;
				uint16_t Actual_Number_Of_Cards = d.GetNumberOfCards();
				Card c = d.PickCard();
				uint16_t Remaining_Cards = d.GetNumberOfCards();
				Assert::IsTrue(Actual_Number_Of_Cards != Remaining_Cards);

			}

			
			TEST_METHOD(Discard)
			{
				Table table;
				Player player1(20, "Alin");
				Player player2(20, "Radu");
				Deck deck;
				deck.DealCards(player1, player2, deck);

				std::vector<Card>originalHand = player1.getHand();
				player1.Discard(originalHand.front(), table);

				std::vector<Card>alteredHand = player1.getHand();

				Assert::AreNotEqual(originalHand.size(), alteredHand.size());

			}

			TEST_METHOD(Score)
			{
				Table table;
				Player player1(20, "Alin");
				Player player2(20, "Radu");

				Card card = Card(Card::Color::Blue, 10);

				table[{0, 1}] = card;

				card = Card(Card::Color::Red, 5);

				table[{14, 4}] = card;

				int16_t scorePlayer1 = 0;
				int16_t scorePlayer2 = 0;

				LostCitiesDuelGame game;
				Assert::IsTrue(game.Score(table,scorePlayer1,scorePlayer2)==true);

			}
		
			TEST_METHOD(PickFromBoard)
			{
				Table table;
				Player player1(20, "Alin");
				uint8_t originalHand = player1.getHand().size();

				table.PutOnBoard(4, Card(Card::Color::Red, 10));
				player1.PickFromBoard(Card::Color::Red, table);

				uint8_t NewHand = player1.getHand().size();
				Assert::IsTrue(table[4]->GetNumber() !=10, L"The card has not been removed from the board!");

				Assert::AreNotEqual(originalHand, NewHand, L"The card hasn't been added to the hand!");


			}

			TEST_METHOD(AddCardToHand)
			{
				Player player1(20, "Raluca");
				uint8_t initialHandSize = player1.getHand().size();
				Card card(Card::Color::Red, 8);

				player1.AddToHand(card);
				uint8_t actualHandSize = player1.getHand().size();

				Assert::AreNotEqual(initialHandSize, actualHandSize, L"The card has not been added to the hand!");

			}


			TEST_METHOD(PlaceCard)
			{
				Player player1(20, "Radu");
				Card card(Card::Color::Red, 8);
				player1.AddToHand(card);
				Table table;

				player1.PlaceCard(0, 4,table, player1.getHand()[0]);

				Assert::IsTrue(table[{0, 4}]->GetNumber() == card.GetNumber(), L"The number of the cards are not equal");

				Assert::IsTrue(table[{0, 4}]->GetColor() == card.GetColor(), L"The color of the cards are not the same!");
			}
	};
}