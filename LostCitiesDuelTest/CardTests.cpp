#include "pch.h"
#include "CppUnitTest.h"
#include "../Lost Cities Duel/Card.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace LostCitiesDuelTest
{
	TEST_CLASS(CardTests)
	{
	public:
		TEST_METHOD(DefaultConstructor)
		{
			Card card(Card::Color::Red, 3);
			Assert::IsTrue(Card::Color::Red == card.GetColor());
			Assert::IsTrue(3 == card.GetNumber());
		}
		
		TEST_METHOD(Constructor)
		{
			Card card(Card::Color::Red, 3);
			Assert::IsTrue(card.GetColor() == Card::Color::Red);
			Assert::IsTrue(card.GetNumber() == 3);

		}

		TEST_METHOD(Print)
		{
			Card card(Card::Color::Red, 3);
			std::stringstream stream;
			stream << card;
			Assert::AreEqual(std::string("Re 3"), stream.str(), L"Card is not the same!");

		}

		TEST_METHOD(GetColor)
		{
			Card card(Card::Color::Red, 3);
			Assert::IsTrue(card.GetColor() == Card::Color::Red , L"can't get color!");
		}

		TEST_METHOD(GetNumber)
		{
			Card card(Card::Color::Red, 3);
			Assert::IsTrue(card.GetNumber() == 3, L"can't get number!");
		}

		TEST_METHOD(Constructorforinvestmentcard)
		{
			Card card(Card::Color::Red, NULL);
			Assert::IsTrue(card.GetColor() == Card::Color::Red);
			Assert::IsTrue(card.GetNumber() == NULL);
		}

		TEST_METHOD(AssigmentOperator)
		{
			Card card1(Card::Color::Red, 3);
			Card card2;
			card2 = card1;
			Assert::IsTrue(card2.GetColor() == card1.GetColor());
			Assert::IsTrue(card2.GetNumber() == card1.GetNumber());
		}
	
		
	};
}
