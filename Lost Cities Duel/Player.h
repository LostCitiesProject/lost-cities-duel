#include<cstdint>
#include<vector>
#include"Card.h"
#include"Deck.h"
#include"Table.h"
#include<algorithm>

class Player
{
public:
	Player();
	Player(uint16_t,std::string);

	std::string GetName();
	uint16_t GetAge();

	static uint16_t ReadAge(std::istream&, uint16_t&);

	void AddToHand(Card& c);
	void PickFromBoard(Card::Color color, Table& table);
	void Discard(Card&,Table&);
	void DisplayHand();
	
	void PlaceCard(uint16_t line, uint16_t column,Table&,Card card);
	Card ChooseCard(std::istream&,Card&);

	std::vector<Card>getHand();

	friend std::ostream& operator<<(std::ostream& out, const Player& player);

private:
	static const uint8_t max_Hand= 8;

private:
	std::string m_name;
	uint16_t m_age;
	std::vector<Card>m_Hand;
};
