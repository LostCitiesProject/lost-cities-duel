#include "Card.h"

Card::Card() : Card(Card::Color::None , NULL)
{
	//empty
}

Card& Card::operator=(const Card& other)
{
	m_color = other.m_color;
	m_number = other.m_number;
	return(*this);
}

Card::Card(const Card& other)
{
	m_color = other.m_color;
	m_number = other.m_number;
}

Card::~Card()
{
	//destructor
}

Card::Card(Color color,const uint16_t & number) :
	m_color(color),m_number(number)
{
	sizeof(*this);
	//static_assert(sizeof(*this) == 1, "this class is not size 1!");
	//std::cout << "first constructor";
}

Card::Card(Color color) : 
	m_color(color),m_number(NULL)
{
	sizeof(*this);
	//static_assert(sizeof(*this) == 1, "this class is not size 1!");
	//std::cout << "second";
}

Card::Color Card::GetColor() const
{
	return m_color;
}
uint16_t Card::GetNumber() const
{
	return m_number;
}


bool Card::operator==(const Card& other)
{
	if (this->m_number == other.m_number && this->m_color == other.m_color) return true;
	return false;
}

std::ostream& operator<<(std::ostream& out, const Card& card)
{
	switch (card.m_color)
	{
	case Card::Color::Red:
		out << "Re ";
		break;
	case Card::Color::Blue:
		out << "Bl ";
		break;
	case Card::Color::Green:
		out << "Gr ";
		break;
	case Card::Color::Yellow:
		out << "Ye ";
		break;
	case Card::Color::White:
		out << "Wh ";
		break;
	case Card::Color::None:
		out << "+-+ ";
		break;
	default:
		throw"Invalid color type!";
	}
	if (card.m_number==NULL)
	{
		if(card.m_color != Card::Color::None)
		out << " I_C ";
	}
	else
	{
		out << card.m_number;
	}

	return out;
}
