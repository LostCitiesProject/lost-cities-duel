#include "LostCitiesDuelGame.h"
#include "Player.h"
#include "Table.h"
#include "Card.h"
#include "Deck.h"

void LostCitiesDuelGame::Run()
{
	std::string playerName;
	uint16_t age;
	std::cout << "Player one name: ";
	std::cin >> playerName;
	std::cout << "Player one age: ";
	age = Player::ReadAge(std::cin, age);
	Player player1(age, playerName);

	std::cout << "Player two name: ";
	std::cin >> playerName;
	std::cout << "Player two age: ";
	age = Player::ReadAge(std::cin, age);
	Player player2(age,playerName);

	Table table;
	int16_t scorePlayerOne=0;
	int16_t scorePlayerTwo=0;
	uint16_t numberOfRounds;
	uint16_t turn = 0;
	bool starter;
	std::cout << "How many rounds do you want to play?\n";



	bool ok = true;
	do {
		try {
			std::cin >> numberOfRounds;
			ok = true;
		}
		catch (std::ios_base::failure & fail) {
			ok = false;
			std::cout << "The number of rounds must be a number!" << std::endl;
			std::cin.clear();
			std::string tmp;
			std::getline(std::cin, tmp);
		}
	} while (ok == false);

	
	for (uint16_t roundNumber = 0; roundNumber < numberOfRounds; roundNumber++)
	{
		Deck deck;
		std::reference_wrapper<Player> activePlayer = player1; 
		std::reference_wrapper<Player> inactivePlayer= player2;

		deck.DealCards(player1, player2, deck);
		if (roundNumber == 0) 
		{
			activePlayer = player1;
			inactivePlayer = player2;
		}
		else
		{
			if (starter == true)
			{
				activePlayer = player1;
				inactivePlayer = player2;
			}
			else
			{
				activePlayer = player2;
				inactivePlayer = player1;
			}
		}
		

		while (deck.GetNumberOfCards() != 0)
		{
			turn++;
			system("cls");
			std::cout << table << '\n';
			std::cout << "Number of cards in deck:" << deck.GetNumberOfCards() << '\n';
			std::cout << "It's " << activePlayer << "'s turn! \n";
			uint16_t option;
			Card card;
			activePlayer.get().DisplayHand();
			std::cout << "Choose your action: \n 1.Put a card on the table \n 2.Discard a card from your hand \n";

			bool ok= true;
			do {
				try {
					std::cin >> option;
					ok = true;
					if (option != 1 && option != 2)
					{
						std::cout << "You can only put a card on the table or discard a card from your hand!\n";
						ok = false;
					}
				}
				catch (std::ios_base::failure & fail) {
					ok = false;
					std::cout << "Choose option 1 or option 2!" << std::endl;
					std::cin.clear();
					std::string tmp;
					std::getline(std::cin, tmp);
				}
			} while (ok == false);

			switch (option)
			{
			case 1:
				while (true)
				{
					try
					{
						std::cout << "Choose a card from your hand(number,color)! \n";
						card = std::move(activePlayer.get().ChooseCard(std::cin,card));
						break;
					}
					catch (const char* message)
					{
						std::cout << message << '\n';
					}
				}
				while (true)
				{
					try
					{ 
						std::cout << "Where do you want to place the card? ";
						break;
					}
					catch (const std::out_of_range & exception)
					{
						std::cout << "***" << exception.what() << '\n';
					}
					catch (const std::logic_error & exception)
					{
						std::cout << exception.what() << '\n';
					}
					catch (const char* message)
					{
						std::cout << message << '\n';
					}
				}
				card = std::move(deck.PickCard());
				activePlayer.get().AddToHand(card);
				break;
			case 2:
				while (true)
				{
					try
					{
						std::cout << "Choose a card from your hand(number,color)! \n";
						card = std::move(activePlayer.get().ChooseCard(std::cin, card));
						break;
					}
					catch (const char* message)
					{
						std::cout << message << '\n';
					}
				}
				activePlayer.get().Discard(card, table);
				std::cout << "Do you want to pick a card from the deck or from the board ? \n 1.Deck \n 2.Board\n";
				do {
					try {
						std::cin >> option;
						ok = true;
						if (option != 1 && option != 2)
						{
							std::cout << "You can only pick cards from the deck or from the board!\n";
							ok = false;
						}
					}
					catch (std::ios_base::failure & fail) {
						ok = false;
						std::cout << "You can only pick cards from the deck or from the board!" << std::endl;
						std::cin.clear();
						std::string tmp;
						std::getline(std::cin, tmp);
					}
				} while (ok == false);
				std::string color;
				switch (option)
				{
				case 1:
					card = std::move(deck.PickCard());
					activePlayer.get().AddToHand(card);
					break;
				case 2:
					while (true)
					{
						std::cout << "From what color do you want to pick?\n";
						std::cin >> color;

						if (color == "Re"&& card.GetColor()!=Card::Color::Red)
						{
							if (card.GetColor() != Card::Color::Red)
							{
								Card::Color c = Card::Color::Red;
								activePlayer.get().PickFromBoard(c, table);
								break;
							}

							else std::cout << "You can't pick a card you just discarded!\n";

						}
						else if (color == "Bl"&& card.GetColor() != Card::Color::Blue)
						{
							if (card.GetColor() != Card::Color::Blue)
							{
								Card::Color c = Card::Color::Blue;
								activePlayer.get().PickFromBoard(c, table);
								break;
							}
							else std::cout << "You can't pick a card you just discarded!\n";

						}
						else if (color == "Ye" && card.GetColor() != Card::Color::Yellow)
						{
							if (card.GetColor() != Card::Color::Yellow)
							{
								Card::Color c = Card::Color::Yellow;
								activePlayer.get().PickFromBoard(c, table);
								break;
							}

							else std::cout << "You can't pick a card you just discarded!\n";
						}
						else if (color == "Wh" && card.GetColor() != Card::Color::White)
						{
							if (card.GetColor() != Card::Color::White)
							{
								Card::Color c = Card::Color::White;
								activePlayer.get().PickFromBoard(c, table);
								break;
							}

							else
							{
								std::cout << "You can't pick a card you just discarded!\n";
								
							}
						}
						else if (color == "Gr")
						{
							if (card.GetColor() != Card::Color::Green)
							{
								Card::Color c = Card::Color::Green;
								activePlayer.get().PickFromBoard(c, table);
								
							}
							else
							{
								std::cout << "You can't pick a card you just discarded!\n";
								
							}
						}
						else 
						{
							std::cout << "That color doesn't exist!\n";
						}
					}

					break;
				default:
					break;
				}
				break;
			
			}

		
			
			std::swap(activePlayer, inactivePlayer);
		}
		starter=Score(table, scorePlayerOne, scorePlayerTwo);
	}

	if (Score(table, scorePlayerOne, scorePlayerTwo)) std::cout << "Player 1 has won!";
	else
		std::cout << "Player 2 has won!";
}

bool LostCitiesDuelGame::Score(const Table& table, int16_t& scorePlayerOne, int16_t& scorePlayerTwo)
{
	uint16_t numberOfCards;
	uint16_t columnScore;
	uint16_t investment;
	uint16_t sizeColumns = 5; 
	uint16_t sizeLines = 24;
	int16_t scorePlayerOneRound = 0;
	int16_t scorePlayerTwoRound = 0;
	for (uint16_t index = 0; index < sizeLines/2; index++)
	{
		numberOfCards = 0;
		columnScore = 0;
		investment = 1;
		for (uint16_t index2 = 0; index2 < sizeColumns; index2++)
		{
			if (table[index, index2]->GetNumber() == NULL)
			{
				investment++;
			}
			else
			{
				numberOfCards++;
				columnScore += table[index, index2]->GetNumber();
			}
		}
		if (numberOfCards != 0 || investment != 1)
		{
			columnScore -= 20;
			columnScore *= investment;
			scorePlayerOneRound += columnScore;
		}
	}
	std::cout << "Player one score for this round is:" << scorePlayerOneRound<<'\n';
	scorePlayerOne += scorePlayerOneRound;

	for (uint16_t index = 0; index < sizeLines / 2; index++)
	{
		numberOfCards = 0;
		columnScore = 0;
		investment = 1;
		for (uint16_t index2 = 0; index2 < sizeColumns; index2++)
		{
			if (table[index+12, index2]->GetNumber() == NULL)
			{
				investment++;
			}
			else
			{
				numberOfCards++;
				columnScore += table[index, index2]->GetNumber();
			}
		}
		if (numberOfCards != 0 || investment != 1)
		{
			columnScore -= 20;
			columnScore *= investment;
			scorePlayerTwoRound += columnScore;
		}
	}
	std::cout << "Player two score for this round is:" << scorePlayerTwoRound<<'\n';
	scorePlayerTwo += scorePlayerTwoRound;
	if (scorePlayerOneRound >= scorePlayerTwoRound)
		return true;
	return false;
}
