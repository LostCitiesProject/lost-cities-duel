#include "Table.h"

Table::Table()
{
	Card card;
	for (uint16_t it = 0; it < 5; it++)
	{
		m_board[it].push(card);
	}
}

const std::optional<Card>& Table::operator[](const Position& position) const
{
	const auto& [line, column] = position;
	if (line < 0 || line >= sizeLines || column < 0 || column >= sizeColumns)
	{
		throw std::out_of_range("Column or line is out of range!");
	}
	
	return m_table[line][column];
}

std::optional<Card>& Table::operator[](const Position& position)
{
	const auto& [line, column] = position;
	return m_table[line][column];
}

const std::optional<Card>& Table::operator[](const uint16_t& position) const
{
	if (position < 0 || position >= sizeColumns)
	{
		throw std::out_of_range("Column or line is out of range!");
	}
	if (m_board[position].top().has_value())
	{
		return m_board[position].top();
	}
}

std::optional<Card>& Table::operator[](const uint16_t& position)
{

	if (m_board[position].top().has_value())
	{
		return m_board[position].top();
	}
}

void Table::RemoveFromBoard(const uint16_t &position)
{
	if (!m_board[position].empty())
		m_board[position].pop();

	if (m_board[position].empty())
		m_board[position].push(Card(Card::Color::None, NULL));



}

void Table::PutOnBoard(const uint16_t& position,Card card)
{
	m_board[position].push(std::move(card));
}



std::ostream& operator<<(std::ostream& out, const Table& table)
{

	for (uint16_t line=0;line<Table::sizeLines;line++)
	{
		for (uint16_t column=0;column<Table::sizeColumns;column++)
		{
			if (table[{line, column}]->GetNumber() != NULL || table[{line, column}]->GetColor() != Card::Color::None)
				out << *table[{line, column}];
			else
				out << "---";
			out << ' ';
			if (line == 11 && column == 4)
			{
				out<<"\n\n";
				out << "\n\n";
				for (auto& colum : table.m_board)
				{
						out << *colum.top();
						out << ' ';
				}
				out << "\n \n";
			}
		}
		out << "\n \n ";
	}
	out << "Ye  Bl   Wh  Gr  Re \n \n";
	return out;
}
