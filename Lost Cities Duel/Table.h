#pragma once

#include <array>
#include <stack>
#include <optional>

#include "Card.h"

class Table
{
public:
	using Position = std::pair<uint16_t, uint16_t>;

public:
	Table();
public:
	const std::optional<Card>& operator[](const Position& position) const;
	std::optional<Card>& operator[](const Position& position);
	const std::optional<Card>& operator[](const uint16_t& position) const;
	std::optional<Card>& operator[](const uint16_t& position);
	friend std::ostream& operator<< (std::ostream& out, const Table& table);

	void RemoveFromBoard(const uint16_t& position);
	void PutOnBoard(const uint16_t& position,Card);

private:
	static const uint16_t sizeColumns = 5;
	static const uint16_t sizeLines = 24;

private:
	std::array<std::array<std::optional<Card>, sizeColumns>, sizeLines> m_table;
	std::array<std::stack<std::optional<Card>>, sizeColumns> m_board;

};

