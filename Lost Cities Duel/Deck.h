#pragma once
#include <stack>
#include <string> 
#include <random>
#include <algorithm>
#include <vector>
#include "Card.h"

class Player;

class Deck
{
public:
	Deck();
	uint16_t GetNumberOfCards();
	void Shuffle();
	void DealCards(Player&, Player&, Deck&);
	Card PickCard();

private:
	void EmplaceCard(const Card& card);

private:
	std::stack <Card> m_deck;
};