#pragma once
#include "Card.h"
#include "Deck.h"
#include "Table.h"




//class Player;

class LostCitiesDuelGame
{
public:
	void Run();
	bool Score(const Table& table, int16_t& scorePlayerOne, int16_t& scorePlayerTwo);
	
};

