#include "Deck.h"
#include<iostream>
#include"Player.h"
Deck::Deck()
{
	Shuffle();
	//std::cout << "Constructor deck \n";
}


uint16_t Deck::GetNumberOfCards()
{
	return m_deck.size();
}

void Deck::EmplaceCard(const Card& card)
{
	m_deck.push(card);
}


void Deck::Shuffle()
{
	std::vector<Card> shuffleVector;
	const uint8_t numberOfColors = 6;
	const uint8_t maxNumber = 10;
	const uint8_t maxHandshakeCards = 3;
	for (uint8_t color = 1; color < numberOfColors; ++color)
	{
		for (uint8_t number = 2; number <= maxNumber; ++number)
		{
			shuffleVector.push_back(Card(static_cast<Card::Color>(color), number));
		}
		for (uint8_t handshakeCard = 0; handshakeCard < maxHandshakeCards; ++handshakeCard)
		{
			shuffleVector.push_back(Card(static_cast<Card::Color>(color)));
		}
	}

	auto rng = std::default_random_engine{};
	std::random_device rd;
	std::mt19937 g(rd());
	std::shuffle(std::begin(shuffleVector), std::end(shuffleVector), g);
	while (!shuffleVector.empty())
	{
		
		Card pushCard = shuffleVector.back();
		EmplaceCard(pushCard);
		shuffleVector.pop_back();
	}
}  

void Deck::DealCards(Player& p1, Player& p2, Deck& deck)
{
	for (uint8_t it = 0; it < 8; it++)
	{
		Card c = deck.PickCard();
		p1.AddToHand(c);
		c = deck.PickCard();
		p2.AddToHand(c);
	}
}

Card Deck::PickCard()
{
	Card pickedCard = m_deck.top();
	m_deck.pop();
	return std::move(pickedCard);
}

