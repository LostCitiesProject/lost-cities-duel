#pragma once
#include "Player.h"
Player::Player(uint16_t age, std::string name) :
	m_age(age), m_name(name)
{

}

Player::Player()
{

}

std::string Player::GetName()
{
	return m_name;
}

uint16_t Player::GetAge()
{
	return m_age;
}



uint16_t Player::ReadAge(std::istream&, uint16_t& age)
{
	bool aux = true;
	std::cin.exceptions(std::istream::failbit);
	do {
		try {
			std::cin >> age;
			aux = true;
		}
		catch (std::ios_base::failure & fail) {
			aux = false;
			std::cout << "Age must be a number!" << std::endl;
			std::cout << "Please enter your age:";
			std::cin.clear();
			std::string tmp;
			std::getline(std::cin, tmp);
		}
	} while (aux == false);

	return age;
}


void Player::AddToHand(Card& c)
{
	m_Hand.push_back(c);
}

void Player::PickFromBoard(Card::Color color, Table& table)
{

	switch (color)
	{
	case Card::Color::Yellow:
	{
		m_Hand.push_back(std::move(table[0].value()));
		table.RemoveFromBoard(0);
		break;
	}
	case Card::Color::Blue:
	{
		m_Hand.push_back(std::move(table[1].value()));
		table.RemoveFromBoard(1);
		break;
	}
	case Card::Color::White:
	{
		m_Hand.push_back(std::move(table[2].value()));
		table.RemoveFromBoard(2);
		break;
	}
	case Card::Color::Green:
	{
		m_Hand.push_back(std::move(table[3].value()));
		table.RemoveFromBoard(3);
		break;
	}
	case Card::Color::Red:
	{
		m_Hand.push_back(std::move(table[4].value()));
		table.RemoveFromBoard(4);
		break;
	}
	}
}



void Player::Discard(Card& C, Table& table)
{
	auto it = std::find(m_Hand.begin(), m_Hand.end(), C);
	uint16_t position = std::distance(m_Hand.begin(), it);
	switch (C.GetColor())
	{
	case Card::Color::Yellow:
		table.PutOnBoard(0, C);
		m_Hand.erase(m_Hand.begin() + position);
		break;

	case Card::Color::Blue:
		table.PutOnBoard(1, C);
		m_Hand.erase(m_Hand.begin() + position);
		break;

	case Card::Color::White:
		table.PutOnBoard(2, C);
		m_Hand.erase(m_Hand.begin() + position);
		break;

	case Card::Color::Green:
		table.PutOnBoard(3, C);
		m_Hand.erase(m_Hand.begin() + position);
		break;

	case Card::Color::Red:
		table.PutOnBoard(4, C);
		m_Hand.erase(m_Hand.begin() + position);
		break;
	}
}
void Player::DisplayHand()
{
	std::copy(m_Hand.begin(), m_Hand.end(),
		std::ostream_iterator<Card>(std::cout, " "));
	std::cout << '\n';
}

void Player::PlaceCard(uint16_t x, uint16_t y, Table& table, Card card)
{
	Table::Position position;
	auto& [line, column] = position;
	uint16_t line2;
	if (x >= 12)
	{
		line = x - 12;
	}
	else
		line = x;
	column = y;
	if (line<12 && line>-1)
	{
		if (x >= 12)
		{
			line = x;
		}
		if (line > 0 && line != 12)
		{
			line2 = line - 1;
			if (table[{line, column}].has_value())







			{
				
				
				
				return;
				throw std::invalid_argument("Position ocuppied!");
			}
			if (column<5 && column>-1)
				while (true)
				{
					if (card.GetColor() != table[{line2, column}]->GetColor())
					{
						throw "Wrong color!";
					}

					else if (table[{line2, column}]->GetNumber() != NULL && card.GetNumber() == NULL)
					{
						return;
						throw "You can't add an investition card!";
					}
					else
					{
						if (card.GetNumber() == NULL && table[{position}]->GetColor() == Card::Color::None)
						{
							table[{position}] = std::move(card);
							auto it = std::find(m_Hand.begin(), m_Hand.end(), card);
							uint16_t pos = std::distance(m_Hand.begin(), it);
							m_Hand.erase(m_Hand.begin() + pos);
							return;
						}
						else if (card.GetNumber() > table[{line2, column}]->GetNumber())
						{
							if (table[{line2, column}]->GetColor() == card.GetColor() && table[{position}]->GetColor() == Card::Color::None)
							{
								table[{position}] = std::move(card);
								auto it = std::find(m_Hand.begin(), m_Hand.end(), card);
								uint16_t pos = std::distance(m_Hand.begin(), it);
								m_Hand.erase(m_Hand.begin() + pos);
								return;
							}
							else
							{
								throw "The color of the card is different";

							}
						}
						else {
							{
								return;
								throw "The vaule of the card is lower then the value of the card on the table!";
							}
							break;

						}
					}
					throw"Column invalid!";
				}
		}
		else
		{
			while (true)

				if (table[{position}]->GetColor() == Card::Color::None)
				{
					if ((card.GetColor() == Card::Color::Yellow && position.second == 0) || (card.GetColor() == Card::Color::Blue && position.second == 1) || (card.GetColor() == Card::Color::White && position.second == 2) || (card.GetColor() == Card::Color::Green && position.second == 3) || (card.GetColor() == Card::Color::Red && position.second == 4))
					{
						table[{position}] = std::move(card);
						auto it = std::find(m_Hand.begin(), m_Hand.end(), card);
						uint16_t pos = std::distance(m_Hand.begin(), it);
						m_Hand.erase(m_Hand.begin() + pos);
						return;

					}
					else
					{
						throw "The color of the card is different";
					}
				}
				else { //throw"Position occupied!"; 
					return; }
		}
	}

	throw"Could not read line or column";
}

Card Player::ChooseCard(std::istream& in, Card& card)
{
	uint16_t cardNumber;
	uint16_t ok;
	std::cout << "Do you want to choose a investment card? \n 1.Yes \n 2.No \n ";

	bool valid = true;
	do {
		try {
			std::cin >> ok;
			valid = true;
			if (ok != 1 && ok != 2) std::cout << "Not a valid option.Choose 1 or 2!\n";
		}
		catch (std::ios_base::failure & fail) {
			valid = false;

			std::cout << "Not a valid option.Choose 1 or 2!" << std::endl;
			std::cin.clear();
			std::string tmp;
			std::getline(std::cin, tmp);
		}
	} while (valid == false && ok != 1 && ok != 2);



	if (ok == 2)
	{
		in >> cardNumber;
	}
	else
		cardNumber = NULL;
	if (in.good())
	{

		std::string cardColor;
		in >> cardColor;

		if (cardColor == "Re") {
			card = Card(Card::Color::Red, cardNumber);
		}

		if (cardColor == "Bl") {
			card = Card(Card::Color::Blue, cardNumber);

		}

		if (cardColor == "Ye") {
			card = Card(Card::Color::Yellow, cardNumber);

		}

		if (cardColor == "Wh") {
			card = Card(Card::Color::White, cardNumber);

		}

		if (cardColor == "Gr") {
			card = Card(Card::Color::Green, cardNumber);

		}

		auto p = std::find(m_Hand.begin(), m_Hand.end(), card);
		if (p != m_Hand.end())
			return card;
		else throw "There is no such card in your hand!";


	}
	throw"Number or color is invalid!";

}

std::vector<Card> Player::getHand()
{
	return m_Hand;
}

std::ostream& operator<<(std::ostream& out, const Player& player)
{
	out << player.m_name;
	return out;
}
