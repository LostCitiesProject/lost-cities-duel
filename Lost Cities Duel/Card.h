#pragma once
#include<iostream>
#include<cstdint>

class Card
{
public:
	enum class Color : uint8_t
	{
		None,
		Red,
		Blue,
		Green,
		Yellow,
		White
	};

public:

	Card();
	Card& operator=(const Card& other);
	Card(const Card& other);
	~Card();
	Card(Color color,const uint16_t& number);
	Card(Color color);
	Color GetColor() const;
	uint16_t GetNumber() const;


	friend std::ostream& operator<<(std::ostream& out, const Card& card);
	bool operator==(const Card& other);


private:

	Color m_color;
	uint16_t m_number;
};


