#include "GameState.h"


//Constructors/Destructors

GameState::GameState(sf::RenderWindow* window, std::stack<State*>* states, std::string p1Name, std::string p2Name, std::string p1Age,
	std::string p2Age) :
	State(window, states)
{
	this->saved_player1Age = p1Age;
	this->saved_player1Name = p1Name;
	this->saved_player1Name[0] = this->saved_player1Name[0] - 32;

	this->saved_player2Age = p2Age;
	this->saved_player2Name = p2Name;
	this->saved_player2Name[0] = this->saved_player2Name[0] - 32;

	this->initBackground();
	this->initFonts();
	this->initTextures();
	this->initPauseMenu();
	this->initTable();
	this->initDeck();
	this->initPlayers();
	this->initCards();
	this->initSpaces();
	this->initDiscardSpaces();
	this->initPlayersName();
	this->initCardsNumberText();

}

GameState::~GameState()
{
	for (auto it = 0; it < cards.size(); ++it)
	{
		delete cards[it];
	}

	for (auto it = 0; it < player1cards.size(); ++it)
	{
		delete player1cards[it];
	}

	for (auto it = 0; it < player2cards.size(); ++it)
	{
		delete player2cards[it];
	}

	for (auto it = 0; it < this->yellow_1.size(); ++it)
	{
		delete yellow_1[it];
	}

	for (auto it = 0; it < this->yellow_2.size(); ++it)
	{
		delete yellow_2[it];
	}

	for (auto it = 0; it < this->green_1.size(); ++it)
	{
		delete green_1[it];
	}

	for (auto it = 0; it < this->green_2.size(); ++it)
	{
		delete green_2[it];
	}

	for (auto it = 0; it < this->blue_1.size(); ++it)
	{
		delete blue_1[it];
	}

	for (auto it = 0; it < this->blue_2.size(); ++it)
	{
		delete blue_2[it];
	}

	for (auto it = 0; it < this->red_1.size(); ++it)
	{
		delete red_1[it];
	}

	for (auto it = 0; it < this->red_2.size(); ++it)
	{
		delete red_2[it];
	}

	for (auto it = 0; it < this->white_1.size(); ++it)
	{
		delete white_1[it];
	}

	for (auto it = 0; it < this->white_2.size(); ++it)
	{
		delete white_2[it];
	}

	for (auto it = 0; it < this->white_discarded.size(); ++it)
	{
		delete white_discarded[it];
	}

	for (auto it = 0; it < this->blue_discarded.size(); ++it)
	{
		delete blue_discarded[it];
	}

	for (auto it = 0; it < this->red_discarded.size(); ++it)
	{
		delete red_discarded[it];
	}

	for (auto it = 0; it < this->green_discarded.size(); ++it)
	{
		delete green_discarded[it];
	}

	for (auto it = 0; it < this->yellow_discarded.size(); ++it)
	{
		delete yellow_discarded[it];
	}

	for (auto it = 0; it < cards.size(); ++it)
	{
		delete cards[it];
	}

	delete deck_graphics;
	delete table;

	delete this->m_Menu;
}


//Functions



void GameState::render(sf::RenderTarget* target)
{
	if (!target)
		target = this->window;
	target->draw(this->background);

	table->render(target);
	this->renderSpaces(target);
	this->renderDiscardSpaces(target);
	/*for (auto it = 0; it < cards.size(); ++it)
	{
		cards[it]->render(target);
	}*/

	this->renderPlayer1(target);

	this->renderPlayer2(target);


	this->deck_graphics->render(target);
	this->renderDiscardCards(target);

	this->renderPlayersName(target);

	this->renderCardsNumberText(target);

	sf::Text mouseText;
	mouseText.setPosition(this->mousePosView.x, this->mousePosView.y - 50);
	mouseText.setFont(this->font);
	mouseText.setCharacterSize(24);
	std::stringstream ss;
	ss << this->mousePosView.x << "    " << mousePosView.y;
	mouseText.setString(ss.str());

	target->draw(mouseText);

	if (this->paused)
	{
		this->m_Menu->render(target);
	}

}

void GameState::renderPlayer1(sf::RenderTarget* target)
{
	if (turn == true) 
	{
		for (auto it = 0; it < player1cards.size(); ++it)
		{
			player1cards[it]->render(target);
		}
	}

	if (yellow_1.size() > 0)
		this->yellow_1[yellow_1.size() - 1]->render(target);

	if (blue_1.size() > 0)
		this->blue_1[blue_1.size() - 1]->render(target);

	if (white_1.size() > 0)
		this->white_1[white_1.size() - 1]->render(target);

	if (green_1.size() > 0)
		this->green_1[green_1.size() - 1]->render(target);

	if (red_1.size() > 0)
		this->red_1[red_1.size() - 1]->render(target);

}

void GameState::renderPlayer2(sf::RenderTarget* target)
{
	if (turn == false)
	{
		for (auto it = 0; it < player2cards.size(); ++it)
		{
			player2cards[it]->render(target);
		}
	}

	if (yellow_2.size() > 0)
		this->yellow_2[yellow_2.size() - 1]->render(target);

	if (blue_2.size() > 0)
		this->blue_2[blue_2.size() - 1]->render(target);

	if (white_2.size() > 0)
		this->white_2[white_2.size() - 1]->render(target);

	if (green_2.size() > 0)
		this->green_2[green_2.size() - 1]->render(target);

	if (red_2.size() > 0)
		this->red_2[red_2.size() - 1]->render(target);
}

void GameState::renderSpaces(sf::RenderTarget* target)
{
	target->draw(this->yellow_space_1);
	target->draw(this->blue_space_1);
	target->draw(this->white_space_1);
	target->draw(this->green_space_1);
	target->draw(this->red_space_1);

	target->draw(this->yellow_space_2);
	target->draw(this->blue_space_2);
	target->draw(this->white_space_2);
	target->draw(this->green_space_2);
	target->draw(this->red_space_2);
}

void GameState::renderDiscardSpaces(sf::RenderTarget* target)
{
	target->draw(this->discard_yellow_space);
	target->draw(this->discard_blue_space);
	target->draw(this->discard_white_space);
	target->draw(this->discard_green_space);
	target->draw(this->discard_red_space);
}

void GameState::renderDiscardCards(sf::RenderTarget* target)
{
	if (this->yellow_discarded.size() > 0)
		this->yellow_discarded[yellow_discarded.size() - 1]->render(target);

	if (blue_discarded.size() > 0)
		this->blue_discarded[blue_discarded.size() - 1]->render(target);

	if (white_discarded.size() > 0)
		this->white_discarded[white_discarded.size() - 1]->render(target);

	if (green_discarded.size() > 0)
		this->green_discarded[green_discarded.size() - 1]->render(target);

	if (red_discarded.size() > 0)
		this->red_discarded[red_discarded.size() - 1]->render(target);
}

void GameState::renderPlayersName(sf::RenderTarget* target)
{
	target->draw(this->player1Name);
	target->draw(this->player2Name);
}

void GameState::renderCardsNumberText(sf::RenderTarget* target)
{
	target->draw(this->cards_number);
}

void GameState::updateInput(const float& dt)
{
	this->updateMousePosition();

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) && this->getKeyTime())
	{
		if (!this->paused)
			this->pauseState();
		else
			this->unpauseState();

	}

}

void GameState::updatePauseMenuButtons()
{
	if (this->m_Menu->isButtonPressed("Quit"))
	{
		this->states->pop();
		this->states->pop();
	}
}

void GameState::update(const float& dt)
{
	this->updateKeyTime(dt);
	this->updateInput(dt);

	if (!this->paused)
	{
		this->updatePlayersName(dt);
		this->updateCardsNumberText(dt);

		if (this->deck_graphics->getCardsNumber() == 0)
		{
			this->updateScore(this->board);

			this->states->push(new FinalState(this->window, this->states,
				this->player1Name, this->player2Name, this->scorePlayer1, this->scorePlayer2));

			this->number_of_rounds--;
		}

		table->update(dt, this->window);
		if (discardCondition == true)
		{
			this->updateInput(dt);
			this->deck_graphics->updateDeck(dt, this->window);

			if (yellow_discarded.size() > 0)
				this->yellow_discarded[yellow_discarded.size() - 1]->update(dt, this->window);

			if (blue_discarded.size() > 0)
				this->blue_discarded[blue_discarded.size() - 1]->update(dt, this->window);

			if (white_discarded.size() > 0)
				this->white_discarded[white_discarded.size() - 1]->update(dt, this->window);

			if (green_discarded.size() > 0)
				this->green_discarded[green_discarded.size() - 1]->update(dt, this->window);

			if (red_discarded.size() > 0)
				this->red_discarded[red_discarded.size() - 1]->update(dt, this->window);

		}
		if (turn == true)
		{
			this->updatePlayer1(dt);
		}
		else
		{
			this->updatePlayer2(dt);
		}
	}
	else
	{
		this->m_Menu->update(this->mousePosView);
		this->updatePauseMenuButtons();
	}
}


void GameState::updateCardsNumberText(const float& dt)
{
	this->cards_n = std::to_string(this->deck_graphics->getCardsNumber());
	this->cards_number.setString(this->cards_n);
}

void GameState::updatePlayersName(const float& dt)
{
	if (turn == true)
	{
		this->player1Name.setOutlineColor(sf::Color::Black);
		this->player2Name.setOutlineColor(sf::Color::Transparent);
	}
	else
	{

		this->player1Name.setOutlineColor(sf::Color::Transparent);
		this->player2Name.setOutlineColor(sf::Color::Black);
	}
}

void GameState::updatePlayer1(const float& dt)
{
	if (this->discardCondition == false)
	{
		if (poz == -1)
			for (int ite = 0; ite < player1cards.size(); ++ite)
			{
				player1cards[ite]->update(dt, this->window);
				if (player1cards[ite]->getMovement() == false)
				{
					poz = ite;
				}
				/*player2cards[poz2]->update(dt, this->window);
				if (player2cards[poz2]->movement == true)
				{
					poz2 = -1;
				}*/
			}
		else
		{
			player1cards[poz]->update(dt, this->window);
			if (player1cards[poz]->getMovement() == true)
			{
				uint16_t c_type = player1cards[poz]->getCardType();
				//&& !(sf::Mouse::isButtonPressed(sf::Mouse::Left))
				if (c_type == card_red)
				{
					if (this->red_space_1.getGlobalBounds().contains(player1cards[poz]->getCoord())
						)
					{
						player1cards[poz]->setPosition(this->red_space_1.getPosition().x, this->red_space_1.getPosition().y);
						player1.PlaceCard(this->placeMarker[4].first, this->placeMarker[4].second, this->board, this->player1cards[poz]->getCard());
						if (board[{this->placeMarker[4].first, this->placeMarker[4].second}].has_value())
						{
							player1cards[poz]->setMove(false);
							this->red_1.push_back(std::move(player1cards[poz]));
							placeMarker[4].first++;
							Card aux = this->deck_graphics->getPickCard();
							player1.AddToHand(aux);
							this->initCards();
							this->turn = false;
							std::cout << "\n \n \n" << board;
							//this->player1cards;
						}
						else
						{
							this->player1cards[poz]->reset();
						}
					}
					else if (this->discard_red_space.getGlobalBounds().contains(player1cards[poz]->getCoord()))
					{
						player1cards[poz]->setPosition(this->discard_red_space.getPosition().x, this->discard_red_space.getPosition().y);
						Card aux = this->player1cards[poz]->getCard();
						player1.Discard(aux, this->board);
						player1cards[poz]->setMove(false);
						this->red_discarded.push_back(std::move(player1cards[poz]));
						this->discardCondition = true;
						std::cout << "\n \n \n" << board;
						this->initCards();
						if (blue_discarded.size() > 0)
							this->blue_discarded[blue_discarded.size() - 1]->setMove(true);

						if (white_discarded.size() > 0)
							this->white_discarded[white_discarded.size() - 1]->setMove(true);

						if (green_discarded.size() > 0)
							this->green_discarded[green_discarded.size() - 1]->setMove(true);

						if (yellow_discarded.size() > 0)
							this->yellow_discarded[yellow_discarded.size() - 1]->setMove(true);
					}
					else
					{
						player1cards[poz]->reset();
					}
				}
				else if (c_type == card_green)
				{
					if (this->green_space_1.getGlobalBounds().contains(player1cards[poz]->getCoord())
						)
					{
						player1cards[poz]->setPosition(this->green_space_1.getPosition().x, this->green_space_1.getPosition().y);
						player1.PlaceCard(this->placeMarker[3].first, this->placeMarker[3].second, this->board, this->player1cards[poz]->getCard());
						if (board[{this->placeMarker[3].first, this->placeMarker[3].second}].has_value())
						{
							player1cards[poz]->setMove(false);
							this->green_1.push_back(std::move(player1cards[poz]));
							placeMarker[3].first++;
							Card aux = this->deck_graphics->getPickCard();
							player1.AddToHand(aux);
							this->initCards();
							turn = false;
							std::cout << "\n \n \n" << board;
							//this->player1cards;
						}
						else
						{
							this->player1cards[poz]->reset();
						}
					}
					else if (this->discard_green_space.getGlobalBounds().contains(player1cards[poz]->getCoord()))
					{
						player1cards[poz]->setPosition(this->discard_green_space.getPosition().x, this->discard_green_space.getPosition().y);
						Card aux = this->player1cards[poz]->getCard();
						player1.Discard(aux, this->board);
						player1cards[poz]->setMove(false);
						this->green_discarded.push_back(std::move(player1cards[poz]));
						this->discardCondition = true;
						this->initCards();

						if (blue_discarded.size() > 0)
							this->blue_discarded[blue_discarded.size() - 1]->setMove(true);

						if (white_discarded.size() > 0)
							this->white_discarded[white_discarded.size() - 1]->setMove(true);

						if (red_discarded.size() > 0)
							this->red_discarded[red_discarded.size() - 1]->setMove(true);

						if (yellow_discarded.size() > 0)
							this->yellow_discarded[yellow_discarded.size() - 1]->setMove(true);
					}
					else
					{
						player1cards[poz]->reset();
					}
				}
				else if (c_type == card_blue)
				{
					if (this->blue_space_1.getGlobalBounds().contains(player1cards[poz]->getCoord())
						)
					{
						player1cards[poz]->setPosition(this->blue_space_1.getPosition().x, this->blue_space_1.getPosition().y);
						player1.PlaceCard(this->placeMarker[1].first, this->placeMarker[1].second, this->board, this->player1cards[poz]->getCard());
						if (board[{this->placeMarker[1].first, this->placeMarker[1].second}].has_value())
						{
							player1cards[poz]->setMove(false);
							this->blue_1.push_back(std::move(player1cards[poz]));
							placeMarker[1].first++;
							Card aux = this->deck_graphics->getPickCard();
							player1.AddToHand(aux);
							this->initCards();
							turn = false;
							std::cout << "\n \n \n" << board;
							//this->player1cards;
						}
						else
						{
							this->player1cards[poz]->reset();
						}
					}
					else if (this->discard_blue_space.getGlobalBounds().contains(player1cards[poz]->getCoord()))
					{
						player1cards[poz]->setPosition(this->discard_blue_space.getPosition().x, this->discard_blue_space.getPosition().y);
						Card aux = this->player1cards[poz]->getCard();
						player1.Discard(aux, this->board);
						player1cards[poz]->setMove(false);
						this->blue_discarded.push_back(std::move(player1cards[poz]));
						this->discardCondition = true;

						this->initCards();

						if (red_discarded.size() > 0)
							this->red_discarded[red_discarded.size() - 1]->setMove(true);

						if (white_discarded.size() > 0)
							this->white_discarded[white_discarded.size() - 1]->setMove(true);

						if (green_discarded.size() > 0)
							this->green_discarded[green_discarded.size() - 1]->setMove(true);

						if (yellow_discarded.size() > 0)
							this->yellow_discarded[yellow_discarded.size() - 1]->setMove(true);
					}
					else
					{
						player1cards[poz]->reset();
					}
				}
				else if (c_type == card_white)
				{
					if (this->white_space_1.getGlobalBounds().contains(player1cards[poz]->getCoord()))
					{
						player1cards[poz]->setPosition(this->white_space_1.getPosition().x, this->white_space_1.getPosition().y);
						player1.PlaceCard(this->placeMarker[2].first, this->placeMarker[2].second, this->board, this->player1cards[poz]->getCard());
						if (board[{this->placeMarker[2].first, this->placeMarker[2].second}].has_value())
						{
							player1cards[poz]->setMove(false);
							this->white_1.push_back(std::move(player1cards[poz]));
							placeMarker[2].first++;
							Card aux = this->deck_graphics->getPickCard();
							player1.AddToHand(aux);
							this->initCards();
							turn = false;
							std::cout << "\n \n \n" << board;
							//this->player1cards;
						}
						else
						{
							this->player1cards[poz]->reset();
						}
					}
					else if (this->discard_white_space.getGlobalBounds().contains(player1cards[poz]->getCoord()))
					{
						player1cards[poz]->setPosition(this->discard_white_space.getPosition().x, this->discard_white_space.getPosition().y);
						Card aux = this->player1cards[poz]->getCard();
						player1.Discard(aux, this->board);
						player1cards[poz]->setMove(false);
						this->white_discarded.push_back(std::move(player1cards[poz]));
						this->discardCondition = true;
						this->initCards();

						if (blue_discarded.size() > 0)
							this->blue_discarded[blue_discarded.size() - 1]->setMove(true);

						if (red_discarded.size() > 0)
							this->red_discarded[red_discarded.size() - 1]->setMove(true);

						if (green_discarded.size() > 0)
							this->green_discarded[green_discarded.size() - 1]->setMove(true);

						if (yellow_discarded.size() > 0)
							this->yellow_discarded[yellow_discarded.size() - 1]->setMove(true);
					}
					else
					{
						player1cards[poz]->reset();
					}
				}
				else if (c_type == card_yellow)
				{
					if (this->yellow_space_1.getGlobalBounds().contains(player1cards[poz]->getCoord().x, player1cards[poz]->getCoord().y))
					{
						player1cards[poz]->setPosition(this->yellow_space_1.getPosition().x, this->yellow_space_1.getPosition().y);
						player1.PlaceCard(this->placeMarker[0].first, this->placeMarker[0].second, this->board, this->player1cards[poz]->getCard());
						if (board[{this->placeMarker[0].first, this->placeMarker[0].second}].has_value())
						{
							player1cards[poz]->setMove(false);
							this->yellow_1.push_back(std::move(player1cards[poz]));
							placeMarker[0].first++;
							Card aux = this->deck_graphics->getPickCard();
							player1.AddToHand(aux);
							this->initCards();
							turn = false;
							std::cout << "\n \n \n" << board;
							//this->player1cards;
						}
						else
						{
							this->player1cards[poz]->reset();
						}
					}
					else if (this->discard_yellow_space.getGlobalBounds().contains(player1cards[poz]->getCoord()))
					{
						player1cards[poz]->setPosition(this->discard_yellow_space.getPosition().x, this->discard_yellow_space.getPosition().y);
						Card aux = this->player1cards[poz]->getCard();
						player1.Discard(aux, this->board);
						player1cards[poz]->setMove(false);
						this->yellow_discarded.push_back(std::move(player1cards[poz]));
						this->discardCondition = true;
						this->initCards();

						if (blue_discarded.size() > 0)
							this->blue_discarded[blue_discarded.size() - 1]->setMove(true);

						if (white_discarded.size() > 0)
							this->white_discarded[white_discarded.size() - 1]->setMove(true);

						if (green_discarded.size() > 0)
							this->green_discarded[green_discarded.size() - 1]->setMove(true);

						if (red_discarded.size() > 0)
							this->red_discarded[red_discarded.size() - 1]->setMove(true);
					}
					else
					{
						player1cards[poz]->reset();
					}
				}
				poz = -1;
			}
		}
	}
	else
	{
		//std::cout << "A intrat in a doua parte";
		if (this->deck_graphics->getUploadStat() == true)
		{
			Card aux = this->deck_graphics->getPickCard();
			player1.AddToHand(aux);
			this->discardCondition = false;
			this->initCards();
			turn = false;
		}
		else
		{
			if (red_discarded.size() > 0)
			{
				if (red_discarded[red_discarded.size() - 1]->getMovementCond() == true &&
					this->discard_red_space.getGlobalBounds().contains(red_discarded[red_discarded.size() - 1]->getCoord()) == false)
				{
					player1.PickFromBoard(Card::Color::Red, this->board);
					this->discardCondition = false;
					this->initCards();
					red_discarded.pop_back();
					turn = false;
				}
			}
			if (green_discarded.size() > 0)
			{
				if (green_discarded[green_discarded.size() - 1]->getMovementCond() == true &&
					this->discard_green_space.getGlobalBounds().contains(green_discarded[green_discarded.size() - 1]->getCoord()) == false)
				{
					player1.PickFromBoard(Card::Color::Green, this->board);
					this->discardCondition = false;
					this->initCards();
					green_discarded.pop_back();
					turn = false;
				}
			}
			if (white_discarded.size() > 0)
			{
				if (white_discarded[white_discarded.size() - 1]->getMovementCond() == true &&
					this->discard_white_space.getGlobalBounds().contains(white_discarded[white_discarded.size() - 1]->getCoord()) == false)
				{
					player1.PickFromBoard(Card::Color::White, this->board);
					this->discardCondition = false;
					this->initCards();
					white_discarded.pop_back();
					turn = false;
				}
			}
			if (blue_discarded.size() > 0)
			{
				if (blue_discarded[blue_discarded.size() - 1]->getMovementCond() == true &&
					this->discard_blue_space.getGlobalBounds().contains(blue_discarded[blue_discarded.size() - 1]->getCoord()) == false)
				{
					player1.PickFromBoard(Card::Color::Blue, this->board);
					this->discardCondition = false;
					this->initCards();
					blue_discarded.pop_back();
					turn = false;
				}
			}
			if (yellow_discarded.size() > 0)
			{
				if (yellow_discarded[yellow_discarded.size() - 1]->getMovementCond() == true &&
					this->discard_yellow_space.getGlobalBounds().contains(yellow_discarded[yellow_discarded.size() - 1]->getCoord()) == false)
				{
					player1.PickFromBoard(Card::Color::Yellow, this->board);
					this->discardCondition = false;
					this->initCards();
					yellow_discarded.pop_back();
					turn = false;
				}
			}
		}
	}
}

void GameState::updatePlayer2(const float& dt)
{
	if (this->discardCondition == false)
	{
		if (poz2 == -1)
			for (int ite = 0; ite < player2cards.size(); ++ite)
			{
				player2cards[ite]->update(dt, this->window);
				if (player2cards[ite]->getMovement() == false)
				{
					poz2 = ite;
				}
			}
		else
		{
			player2cards[poz2]->update(dt, this->window);
			if (player2cards[poz2]->getMovement() == true)
			{
				uint16_t c_type = player2cards[poz2]->getCardType();
				//&& !(sf::Mouse::isButtonPressed(sf::Mouse::Left))
				if (c_type == card_red)
				{
					if (this->red_space_2.getGlobalBounds().contains(player2cards[poz2]->getCoord())
						)
					{
						player2cards[poz2]->setPosition(this->red_space_2.getPosition().x, this->red_space_2.getPosition().y);
						player2.PlaceCard(this->placeMarker2[4].first, this->placeMarker2[4].second, this->board, this->player2cards[poz2]->getCard());
						if (board[{this->placeMarker2[4].first, this->placeMarker2[4].second}].has_value())
						{
							player2cards[poz2]->setMove(false);
							this->red_2.push_back(std::move(player2cards[poz2]));
							placeMarker2[4].first++;
							Card aux = this->deck_graphics->getPickCard();
							player2.AddToHand(aux);
							this->initCards();
							std::cout << "\n \n \n" << board;
							turn = true;
							//this->player1cards;
						}
						else
						{
							this->player2cards[poz2]->reset();
						}
					}
					else if (this->discard_red_space.getGlobalBounds().contains(player2cards[poz2]->getCoord()))
					{
						player2cards[poz2]->setPosition(this->discard_red_space.getPosition().x, this->discard_red_space.getPosition().y);
						Card aux = this->player2cards[poz2]->getCard();
						player2.Discard(aux, this->board);
						player2cards[poz2]->setMove(false);
						this->red_discarded.push_back(std::move(player2cards[poz2]));
						this->discardCondition = true;
						this->initCards();
						std::cout << "\n \n \n" << board;
						if (blue_discarded.size() > 0)
							this->blue_discarded[blue_discarded.size() - 1]->setMove(true);

						if (white_discarded.size() > 0)
							this->white_discarded[white_discarded.size() - 1]->setMove(true);

						if (green_discarded.size() > 0)
							this->green_discarded[green_discarded.size() - 1]->setMove(true);

						if (yellow_discarded.size() > 0)
							this->yellow_discarded[yellow_discarded.size() - 1]->setMove(true);
					}
					else
					{
						player2cards[poz2]->reset();
					}
				}
				else if (c_type == card_green)
				{
					if (this->green_space_2.getGlobalBounds().contains(player2cards[poz2]->getCoord())
						)
					{
						player2cards[poz2]->setPosition(this->green_space_2.getPosition().x, this->green_space_2.getPosition().y);
						player2.PlaceCard(this->placeMarker2[3].first, this->placeMarker2[3].second, this->board, this->player2cards[poz2]->getCard());
						if (board[{this->placeMarker2[3].first, this->placeMarker2[3].second}].has_value())
						{
							player2cards[poz2]->setMove(false);
							this->green_2.push_back(std::move(player2cards[poz2]));
							placeMarker2[3].first++;
							Card aux = this->deck_graphics->getPickCard();
							player2.AddToHand(aux);
							this->initCards();
							std::cout << "\n \n \n" << board;
							turn = true;
							//this->player1cards;
						}
						else
						{
							this->player2cards[poz2]->reset();
						}
					}
					else if (this->discard_green_space.getGlobalBounds().contains(player2cards[poz2]->getCoord()))
					{
						player2cards[poz2]->setPosition(this->discard_green_space.getPosition().x, this->discard_green_space.getPosition().y);
						Card aux = this->player2cards[poz2]->getCard();
						player2.Discard(aux, this->board);
						player2cards[poz2]->setMove(false);
						this->green_discarded.push_back(std::move(player2cards[poz2]));
						this->discardCondition = true;
						this->initCards();
						std::cout << "\n \n \n" << board;
						if (blue_discarded.size() > 0)
							this->blue_discarded[blue_discarded.size() - 1]->setMove(true);

						if (white_discarded.size() > 0)
							this->white_discarded[white_discarded.size() - 1]->setMove(true);

						if (red_discarded.size() > 0)
							this->red_discarded[red_discarded.size() - 1]->setMove(true);

						if (yellow_discarded.size() > 0)
							this->yellow_discarded[yellow_discarded.size() - 1]->setMove(true);
					}
					else
					{
						player2cards[poz2]->reset();
					}
				}
				else if (c_type == card_blue)
				{
					if (this->blue_space_2.getGlobalBounds().contains(player2cards[poz2]->getCoord())
						)
					{
						player2cards[poz2]->setPosition(this->blue_space_2.getPosition().x, this->blue_space_2.getPosition().y);
						player2.PlaceCard(this->placeMarker2[1].first, this->placeMarker2[1].second, this->board, this->player2cards[poz2]->getCard());
						if (board[{this->placeMarker2[1].first, this->placeMarker2[1].second}].has_value())
						{
							player2cards[poz2]->setMove(false);
							this->blue_2.push_back(std::move(player2cards[poz2]));
							placeMarker2[1].first++;
							Card aux = this->deck_graphics->getPickCard();
							player2.AddToHand(aux);
							this->initCards();
							turn = true;
							std::cout << "\n \n \n" << board;
							//this->player1cards;
						}

						else
						{
							this->player2cards[poz2]->reset();
						}
					}
					else if (this->discard_blue_space.getGlobalBounds().contains(player2cards[poz2]->getCoord()))
					{
						player2cards[poz2]->setPosition(this->discard_blue_space.getPosition().x, this->discard_blue_space.getPosition().y);
						Card aux = this->player2cards[poz2]->getCard();
						player2.Discard(aux, this->board);
						player2cards[poz2]->setMove(false);
						this->blue_discarded.push_back(std::move(player2cards[poz2]));
						this->discardCondition = true;
						this->initCards();
						if (red_discarded.size() > 0)
							this->red_discarded[red_discarded.size() - 1]->setMove(true);

						if (white_discarded.size() > 0)
							this->white_discarded[white_discarded.size() - 1]->setMove(true);

						if (green_discarded.size() > 0)
							this->green_discarded[green_discarded.size() - 1]->setMove(true);

						if (yellow_discarded.size() > 0)
							this->yellow_discarded[yellow_discarded.size() - 1]->setMove(true);
					}
					else
					{
						player2cards[poz2]->reset();
					}
				}
				else if (c_type == card_white)
				{
					if (this->white_space_2.getGlobalBounds().contains(player2cards[poz2]->getCoord())
						)
					{
						player2cards[poz2]->setPosition(this->white_space_2.getPosition().x, this->white_space_2.getPosition().y);
						player2.PlaceCard(this->placeMarker2[2].first, this->placeMarker2[2].second, this->board, this->player2cards[poz2]->getCard());
						if (board[{this->placeMarker2[2].first, this->placeMarker2[2].second}].has_value())
						{
							player2cards[poz2]->setMove(false);
							this->white_2.push_back(std::move(player2cards[poz2]));
							placeMarker2[2].first++;
							Card aux = this->deck_graphics->getPickCard();
							player2.AddToHand(aux);
							this->initCards();
							turn = true;
							std::cout << "\n \n \n" << board;
							//this->player1cards;
						}

						else
						{
							this->player2cards[poz2]->reset();
						}
					}
					else if (this->discard_white_space.getGlobalBounds().contains(player2cards[poz2]->getCoord()))
					{
						player2cards[poz2]->setPosition(this->discard_white_space.getPosition().x, this->discard_white_space.getPosition().y);
						Card aux = this->player2cards[poz2]->getCard();
						player2.Discard(aux, this->board);
						player2cards[poz2]->setMove(false);
						this->white_discarded.push_back(std::move(player2cards[poz2]));
						this->discardCondition = true;
						this->initCards();
						if (blue_discarded.size() > 0)
							this->blue_discarded[blue_discarded.size() - 1]->setMove(true);

						if (red_discarded.size() > 0)
							this->red_discarded[red_discarded.size() - 1]->setMove(true);

						if (green_discarded.size() > 0)
							this->green_discarded[green_discarded.size() - 1]->setMove(true);

						if (yellow_discarded.size() > 0)
							this->yellow_discarded[yellow_discarded.size() - 1]->setMove(true);
					}
					else
					{
						player2cards[poz2]->reset();
					}
				}
				else if (c_type == card_yellow)
				{
					if (this->yellow_space_2.getGlobalBounds().contains(player2cards[poz2]->getCoord().x, player2cards[poz2]->getCoord().y)
						)
					{
						player2cards[poz2]->setPosition(this->yellow_space_2.getPosition().x, this->yellow_space_2.getPosition().y);
						player2.PlaceCard(this->placeMarker2[0].first, this->placeMarker2[0].second, this->board, this->player2cards[poz2]->getCard());
						if (board[{this->placeMarker2[0].first, this->placeMarker2[0].second}].has_value())
						{
							player2cards[poz2]->setMove(false);
							this->yellow_2.push_back(std::move(player2cards[poz2]));
							placeMarker2[0].first++;
							Card aux = this->deck_graphics->getPickCard();
							player2.AddToHand(aux);
							this->initCards();
							turn = true;
							std::cout << "\n \n \n" << board;
							//this->player1cards;
						}
						else
						{
							this->player2cards[poz2]->reset();
						}
					}
					else if (this->discard_yellow_space.getGlobalBounds().contains(player2cards[poz2]->getCoord()))
					{
						player2cards[poz2]->setPosition(this->discard_yellow_space.getPosition().x, this->discard_yellow_space.getPosition().y);
						Card aux = this->player2cards[poz2]->getCard();
						player2.Discard(aux, this->board);
						player2cards[poz2]->setMove(false);
						this->yellow_discarded.push_back(std::move(player2cards[poz2]));
						this->discardCondition = true;
						this->initCards();
						if (blue_discarded.size() > 0)
							this->blue_discarded[blue_discarded.size() - 1]->setMove(true);

						if (white_discarded.size() > 0)
							this->white_discarded[white_discarded.size() - 1]->setMove(true);

						if (green_discarded.size() > 0)
							this->green_discarded[green_discarded.size() - 1]->setMove(true);

						if (red_discarded.size() > 0)
							this->red_discarded[red_discarded.size() - 1]->setMove(true);
					}
					else
					{
						player2cards[poz2]->reset();
					}
				}
				poz2 = -1;
			}
		}
	}
	else
	{
		//std::cout << "A intrat in a doua parte";
		if (this->deck_graphics->getUploadStat() == true)
		{
			Card aux = this->deck_graphics->getPickCard();
			player2.AddToHand(aux);
			this->discardCondition = false;
			this->initCards();
			turn = true;
		}
		else
		{
			if (red_discarded.size() > 0)
			{
				if (red_discarded[red_discarded.size() - 1]->getMovementCond() == true &&
					this->discard_red_space.getGlobalBounds().contains(red_discarded[red_discarded.size() - 1]->getCoord()) == false)
				{
					player2.PickFromBoard(Card::Color::Red, this->board);
					this->discardCondition = false;
					this->initCards();
					red_discarded.pop_back();
					turn = true;
					std::cout << "\n \n \n" << board;
				}
			}
			if (green_discarded.size() > 0)
			{
				if (green_discarded[green_discarded.size() - 1]->getMovementCond() == true &&
					this->discard_green_space.getGlobalBounds().contains(green_discarded[green_discarded.size() - 1]->getCoord()) == false)
				{
					player2.PickFromBoard(Card::Color::Green, this->board);
					this->discardCondition = false;
					this->initCards();
					green_discarded.pop_back();
					turn = true;
					std::cout << "\n \n \n" << board;
				}
			}
			if (white_discarded.size() > 0)
			{
				if (white_discarded[white_discarded.size() - 1]->getMovementCond() == true &&
					this->discard_white_space.getGlobalBounds().contains(white_discarded[white_discarded.size() - 1]->getCoord()) == false)
				{
					player2.PickFromBoard(Card::Color::White, this->board);
					this->discardCondition = false;
					this->initCards();
					white_discarded.pop_back();
					turn = true;
					std::cout << "\n \n \n" << board;
				}
			}
			if (blue_discarded.size() > 0)
			{
				if (blue_discarded[blue_discarded.size() - 1]->getMovementCond() == true &&
					this->discard_blue_space.getGlobalBounds().contains(blue_discarded[blue_discarded.size() - 1]->getCoord()) == false)
				{
					player2.PickFromBoard(Card::Color::Blue, this->board);
					this->discardCondition = false;
					this->initCards();
					blue_discarded.pop_back();
					turn = true;
					std::cout << "\n \n \n" << board;
				}
			}
			if (yellow_discarded.size() > 0)
			{
				if (yellow_discarded[yellow_discarded.size() - 1]->getMovementCond() == true &&
					this->discard_yellow_space.getGlobalBounds().contains(yellow_discarded[yellow_discarded.size() - 1]->getCoord()) == false)
				{
					player2.PickFromBoard(Card::Color::Yellow, this->board);
					this->discardCondition = false;
					this->initCards();
					yellow_discarded.pop_back();
					turn = true;
					std::cout << "\n \n \n" << board;
				}
			}
		}
	}
}

//Initialization
void GameState::initTextures()
{
	sf::Texture temp;
	temp.loadFromFile("../LostCitiesDuelGraphic/Resources/Images/Card/back.jpg");
	this->textures["CARD_BACK"] = temp;
	temp.loadFromFile("../LostCitiesDuelGraphic/Resources/Images/Card/whiteNormal.jpg");
	this->textures["WHITE_NORMAL"] = temp;
	temp.loadFromFile("../LostCitiesDuelGraphic/Resources/Images/Card/whiteInv.jpg");
	this->textures["WHITE_INV"] = temp;
	temp.loadFromFile("../LostCitiesDuelGraphic/Resources/Images/Card/greenNormal.jpg");
	this->textures["GREEN_NORMAL"] = temp;
	temp.loadFromFile("../LostCitiesDuelGraphic/Resources/Images/Card/greenInv.jpg");
	this->textures["GREEN_INV"] = temp;
	temp.loadFromFile("../LostCitiesDuelGraphic/Resources/Images/Card/blueNormal.jpg");
	this->textures["BLUE_NORMAL"] = temp;
	temp.loadFromFile("../LostCitiesDuelGraphic/Resources/Images/Card/blueInv.jpg");
	this->textures["BLUE_INV"] = temp;
	temp.loadFromFile("../LostCitiesDuelGraphic/Resources/Images/Card/redNormal.jpg");
	this->textures["RED_NORMAL"] = temp;
	temp.loadFromFile("../LostCitiesDuelGraphic/Resources/Images/Card/redInv.jpg");
	this->textures["RED_INV"] = temp;
	temp.loadFromFile("../LostCitiesDuelGraphic/Resources/Images/Card/yellowNormal.jpg");
	this->textures["YELLOW_NORMAL"] = temp;
	temp.loadFromFile("../LostCitiesDuelGraphic/Resources/Images/Card/yellowInv.jpg");
	this->textures["YELLOW_INV"] = temp;
	temp.loadFromFile("../LostCitiesDuelGraphic/Resources/Images/Table/table.jpg");
	this->textures["TABLE"] = temp;
}

void GameState::initCards()
{
	////delete card;
	//card = new CardGraphics(100, 0, &this->textures["WHITE_NORMAL"], true);
	//cards.push_back(card);
	////delete card;
	//card = new CardGraphics(200, 0, &this->textures["WHITE_INV"], true);
	//cards.push_back(card);
	////delete card;
	//card = new CardGraphics(300, 0, &this->textures["GREEN_NORMAL"], true);
	//cards.push_back(card);
	////delete card;
	//card = new CardGraphics(400, 0, &this->textures["GREEN_INV"], true);
	//cards.push_back(card);
	////delete card;
	//card = new CardGraphics(500, 0, &this->textures["RED_NORMAL"], true);
	//cards.push_back(card);
	////delete card;
	//card = new CardGraphics(600, 0, &this->textures["RED_INV"], true);
	//cards.push_back(card);
	////delete card;
	//card = new CardGraphics(700, 0, &this->textures["YELLOW_NORMAL"], true);
	//cards.push_back(card);
	////delete card;
	//card = new CardGraphics(800, 0, &this->textures["YELLOW_INV"], true);
	//cards.push_back(card);
	////delete card;
	//card = new CardGraphics(900, 0, &this->textures["BLUE_NORMAL"], true);
	//cards.push_back(card);
	////delete card;
	//card = new CardGraphics(1000, 0, &this->textures["BLUE_INV"], true);
	//cards.push_back(card);
	////delete card;
	CardGraphics* card;// = new CardGraphics(0, 0, &this->textures["CARD_BACK"]);
	//card = new CardGraphics(1700, 375, &this->textures["CARD_BACK"], false,card_back);
	//cards.push_back(card);

	this->player1cards.clear();
	this->player2cards.clear();

	if (discardCondition == false)
	{
		for (int i = 0; i < 8; i++)
		{
			int number = player1.getHand()[i].GetNumber();
			Card::Color color = player1.getHand()[i].GetColor();

			if (number != NULL)
			{
				sf::Text text_aux;
				text_aux.setString(std::to_string(number));
				text_aux.setCharacterSize(44);
				text_aux.setFont(this->font);
				text_aux.setFillColor(sf::Color::Black);
				text_aux.setOutlineColor(sf::Color::White);
				text_aux.setOutlineThickness(5);
				switch (color)
				{
				case Card::Color::None:
					break;

				case Card::Color::Red:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["RED_NORMAL"],
						true, text_aux, card_red, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::Blue:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["BLUE_NORMAL"],
						true, text_aux, card_blue, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::Green:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["GREEN_NORMAL"],
						true, text_aux, card_green, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::Yellow:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["YELLOW_NORMAL"],
						true, text_aux, card_yellow, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::White:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["WHITE_NORMAL"],
						true, text_aux, card_white, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				default:
					break;
				}
			}
			else
			{
				switch (color)
				{
				case Card::Color::None:
					break;

				case Card::Color::Red:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["RED_INV"],
						true, card_red, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::Blue:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["BLUE_INV"],
						true, card_blue, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::Green:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["GREEN_INV"],
						true, card_green, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::Yellow:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["YELLOW_INV"],
						true, card_yellow, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::White:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["WHITE_INV"],
						true, card_white, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				default:
					break;
				}
			}
		}

		for (int i = 0; i < 8; i++)
		{
			int number = player2.getHand()[i].GetNumber();
			Card::Color color = player2.getHand()[i].GetColor();

			if (number != NULL)
			{
				sf::Text text_aux;
				text_aux.setString(std::to_string(number));
				text_aux.setCharacterSize(44);
				text_aux.setFont(this->font);
				text_aux.setFillColor(sf::Color::Black);
				text_aux.setOutlineColor(sf::Color::White);
				text_aux.setOutlineThickness(5);
				switch (color)
				{
				case Card::Color::None:
					break;

				case Card::Color::Red:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["RED_NORMAL"],
						true, text_aux, card_red, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::Blue:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["BLUE_NORMAL"],
						true, text_aux, card_blue, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::Green:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["GREEN_NORMAL"],
						true, text_aux, card_green, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::Yellow:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["YELLOW_NORMAL"],
						true, text_aux, card_yellow, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::White:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["WHITE_NORMAL"],
						true, text_aux, card_white, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				default:
					break;
				}
			}
			else
			{
				switch (color)
				{
				case Card::Color::None:
					break;

				case Card::Color::Red:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["RED_INV"],
						true, card_red, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::Blue:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["BLUE_INV"],
						true, card_blue, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::Green:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["GREEN_INV"],
						true, card_green, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::Yellow:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["YELLOW_INV"],
						true, card_yellow, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::White:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["WHITE_INV"],
						true, card_white, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				default:
					break;
				}
			}
		}
	}
	else if (turn == true)
	{
		for (int i = 0; i < 7; i++)
		{
			int number = player1.getHand()[i].GetNumber();
			Card::Color color = player1.getHand()[i].GetColor();

			if (number != NULL)
			{
				sf::Text text_aux;
				text_aux.setString(std::to_string(number));
				text_aux.setCharacterSize(44);
				text_aux.setFont(this->font);
				text_aux.setFillColor(sf::Color::Black);
				text_aux.setOutlineColor(sf::Color::White);
				text_aux.setOutlineThickness(5);
				switch (color)
				{
				case Card::Color::None:
					break;

				case Card::Color::Red:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["RED_NORMAL"],
						true, text_aux, card_red, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::Blue:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["BLUE_NORMAL"],
						true, text_aux, card_blue, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::Green:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["GREEN_NORMAL"],
						true, text_aux, card_green, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::Yellow:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["YELLOW_NORMAL"],
						true, text_aux, card_yellow, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::White:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["WHITE_NORMAL"],
						true, text_aux, card_white, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				default:
					break;
				}
			}
			else
			{
				switch (color)
				{
				case Card::Color::None:
					break;

				case Card::Color::Red:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["RED_INV"],
						true, card_red, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::Blue:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["BLUE_INV"],
						true, card_blue, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::Green:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["GREEN_INV"],
						true, card_green, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::Yellow:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["YELLOW_INV"],
						true, card_yellow, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::White:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["WHITE_INV"],
						true, card_white, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				default:
					break;
				}
			}
		}
		for (int i = 0; i < 8; i++)
		{
			int number = player2.getHand()[i].GetNumber();
			Card::Color color = player2.getHand()[i].GetColor();

			if (number != NULL)
			{
				sf::Text text_aux;
				text_aux.setString(std::to_string(number));
				text_aux.setCharacterSize(44);
				text_aux.setFont(this->font);
				text_aux.setFillColor(sf::Color::Black);
				text_aux.setOutlineColor(sf::Color::White);
				text_aux.setOutlineThickness(5);
				switch (color)
				{
				case Card::Color::None:
					break;

				case Card::Color::Red:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["RED_NORMAL"],
						true, text_aux, card_red, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::Blue:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["BLUE_NORMAL"],
						true, text_aux, card_blue, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::Green:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["GREEN_NORMAL"],
						true, text_aux, card_green, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::Yellow:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["YELLOW_NORMAL"],
						true, text_aux, card_yellow, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::White:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["WHITE_NORMAL"],
						true, text_aux, card_white, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				default:
					break;
				}
			}
			else
			{
				switch (color)
				{
				case Card::Color::None:
					break;

				case Card::Color::Red:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["RED_INV"],
						true, card_red, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::Blue:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["BLUE_INV"],
						true, card_blue, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::Green:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["GREEN_INV"],
						true, card_green, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::Yellow:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["YELLOW_INV"],
						true, card_yellow, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::White:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["WHITE_INV"],
						true, card_white, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				default:
					break;
				}
			}
		}
	}
	else
	{
		for (int i = 0; i < 8; i++)
		{
			int number = player1.getHand()[i].GetNumber();
			Card::Color color = player1.getHand()[i].GetColor();

			if (number != NULL)
			{
				sf::Text text_aux;
				text_aux.setString(std::to_string(number));
				text_aux.setCharacterSize(44);
				text_aux.setFont(this->font);
				text_aux.setFillColor(sf::Color::Black);
				text_aux.setOutlineColor(sf::Color::White);
				text_aux.setOutlineThickness(5);
				switch (color)
				{
				case Card::Color::None:
					break;

				case Card::Color::Red:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["RED_NORMAL"],
						true, text_aux, card_red, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::Blue:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["BLUE_NORMAL"],
						true, text_aux, card_blue, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::Green:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["GREEN_NORMAL"],
						true, text_aux, card_green, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::Yellow:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["YELLOW_NORMAL"],
						true, text_aux, card_yellow, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::White:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["WHITE_NORMAL"],
						true, text_aux, card_white, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				default:
					break;
				}
			}
			else
			{
				switch (color)
				{
				case Card::Color::None:
					break;

				case Card::Color::Red:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["RED_INV"],
						true, card_red, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::Blue:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["BLUE_INV"],
						true, card_blue, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::Green:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["GREEN_INV"],
						true, card_green, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::Yellow:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["YELLOW_INV"],
						true, card_yellow, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				case Card::Color::White:

					card = new CardGraphics(this->player1HandPoz[i].first, this->player1HandPoz[i].second, &this->textures["WHITE_INV"],
						true, card_white, player1.getHand()[i]);
					player1cards.push_back(card);
					break;

				default:
					break;
				}
			}
		}
		for (int i = 0; i < 7; i++)
		{
			int number = player2.getHand()[i].GetNumber();
			Card::Color color = player2.getHand()[i].GetColor();

			if (number != NULL)
			{
				sf::Text text_aux;
				text_aux.setString(std::to_string(number));
				text_aux.setCharacterSize(44);
				text_aux.setFont(this->font);
				text_aux.setFillColor(sf::Color::Black);
				text_aux.setOutlineColor(sf::Color::White);
				text_aux.setOutlineThickness(5);
				switch (color)
				{
				case Card::Color::None:
					break;

				case Card::Color::Red:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["RED_NORMAL"],
						true, text_aux, card_red, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::Blue:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["BLUE_NORMAL"],
						true, text_aux, card_blue, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::Green:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["GREEN_NORMAL"],
						true, text_aux, card_green, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::Yellow:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["YELLOW_NORMAL"],
						true, text_aux, card_yellow, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::White:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["WHITE_NORMAL"],
						true, text_aux, card_white, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				default:
					break;
				}
			}
			else
			{
				switch (color)
				{
				case Card::Color::None:
					break;

				case Card::Color::Red:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["RED_INV"],
						true, card_red, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::Blue:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["BLUE_INV"],
						true, card_blue, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::Green:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["GREEN_INV"],
						true, card_green, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::Yellow:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["YELLOW_INV"],
						true, card_yellow, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				case Card::Color::White:

					card = new CardGraphics(this->player2HandPoz[i].first, this->player2HandPoz[i].second, &this->textures["WHITE_INV"],
						true, card_white, player2.getHand()[i]);
					player2cards.push_back(card);
					break;

				default:
					break;
				}
			}
		}
	}
}

void GameState::initTable()
{
	table = new TableGraphics(500, 350, &this->textures["TABLE"], false);
}

void GameState::initDeck()
{

	// = new CardGraphics(0, 0, &this->textures["CARD_BACK"]);
	//card = new CardGraphics(1700, 375, &this->textures["CARD_BACK"], false,card_back);
	this->deck_graphics = new DeckGraphics(1700, 375, &this->textures["CARD_BACK"]);
}

void GameState::initBackground()
{
	this->background.setSize(
		sf::Vector2f
		(
			static_cast<float>(this->window->getSize().x),
			static_cast<float>(this->window->getSize().y)
		)
	);
	if (!this->backgroundTexture.loadFromFile("../LostCitiesDuelGraphic/Resources/Images/Background/bg2.png"))
	{
		throw "ERROR:MAIN_MENU_STATE::FAILED_TO_LOAD_BACKGROUND_TEXTURE";
	}
	this->background.setTexture(&this->backgroundTexture);
}

void GameState::initPauseMenu()
{
	this->m_Menu = new PauseMenu(*this->window, this->font);

	this->m_Menu->addButton("Quit", 800.f, "Return to menu");
}

void GameState::initPlayers()
{
	std::string nume;
	int a, a2;

	std::stringstream aux(this->saved_player1Age);
	std::stringstream aux2(this->saved_player2Age);
	aux >> a;
	aux2 >> a2;
	if (a >= a2)
	{
		nume = this->saved_player1Name;
		this->age = a;
		this->player1 = Player(this->age, nume);
		this->age1 = a2;
		nume = this->saved_player2Name;
		this->player2 = Player(this->age1, nume);
	}
	else
	{
		this->age = a2;
		nume = this->saved_player1Name;
		this->player2 = Player(this->age, nume);
		this->age1 = a;
		nume = this->saved_player2Name;
		this->player1 = Player(this->age1, nume);
	}

	aux2 = std::stringstream(this->t_rounds_enter.getString());
	aux2 >> a;
	this->numberOfRounds = a;

	//pozitiile cartilor mana jucator 1
	int x = 480;
	int y = 0;
	for (int i = 0; i < 8; i++)
	{
		std::pair<int, int> p;
		p.first = x;
		p.second = y;
		this->player1HandPoz.push_back(p);
		x += 120;
	}

	std::cout << "\n";
	std::cout << this->deck_graphics->getCardsNumber();
	//pozitiile cartilor mana jucator 2
	x = 480;
	y = 880;
	for (int i = 0; i < 8; i++)
	{
		std::pair<int, int> p;
		p.first = x;
		p.second = y;
		this->player2HandPoz.push_back(p);
		x += 120;
	}


	this->deck_graphics->dealCardsG(this->player1, this->player2);
	std::cout << this->deck_graphics->getCardsNumber();

	for (int i = 0; i < 5; i++)
	{
		std::pair<int, int> p;
		p.first = 0;
		p.second = i;
		this->placeMarker.push_back(p);

		p.first = 12;
		this->placeMarker2.push_back(p);
	}

}

void GameState::initSpaces()
{
	int x = 547;
	int y = 182;

	for (int i = 0; i < 5; i++)
	{
		std::pair<int, int> aux;
		aux.first = x;
		aux.second = y;
		this->space1Poz.push_back(aux);
		x += 180;
	}

	x = 547;
	y = 590;

	for (int i = 0; i < 5; i++)
	{
		std::pair<int, int> aux;
		aux.first = x;
		aux.second = y;
		this->space2Poz.push_back(aux);
		x += 180;
	}

	this->yellow_space_1.setPosition(this->space1Poz[0].first, this->space1Poz[0].second);
	this->yellow_space_1.setSize(sf::Vector2f(110, 170));
	this->yellow_space_1.setOutlineColor(sf::Color::White);
	this->yellow_space_1.setOutlineThickness(5);
	this->yellow_space_1.setFillColor(sf::Color::Transparent);

	this->yellow_space_2.setPosition(this->space2Poz[0].first, this->space2Poz[0].second);
	this->yellow_space_2.setSize(sf::Vector2f(110, 170));
	this->yellow_space_2.setOutlineColor(sf::Color::White);
	this->yellow_space_2.setOutlineThickness(5);
	this->yellow_space_2.setFillColor(sf::Color::Transparent);

	this->blue_space_1.setPosition(this->space1Poz[1].first, this->space1Poz[1].second);
	this->blue_space_1.setSize(sf::Vector2f(110, 170));
	this->blue_space_1.setOutlineColor(sf::Color::White);
	this->blue_space_1.setOutlineThickness(5);
	this->blue_space_1.setFillColor(sf::Color::Transparent);

	this->blue_space_2.setPosition(this->space2Poz[1].first, this->space2Poz[1].second);
	this->blue_space_2.setSize(sf::Vector2f(110, 170));
	this->blue_space_2.setOutlineColor(sf::Color::White);
	this->blue_space_2.setOutlineThickness(5);
	this->blue_space_2.setFillColor(sf::Color::Transparent);

	this->white_space_1.setPosition(this->space1Poz[2].first, this->space1Poz[2].second);
	this->white_space_1.setSize(sf::Vector2f(110, 170));
	this->white_space_1.setOutlineColor(sf::Color::White);
	this->white_space_1.setOutlineThickness(5);
	this->white_space_1.setFillColor(sf::Color::Transparent);

	this->white_space_2.setPosition(this->space2Poz[2].first, this->space2Poz[2].second);
	this->white_space_2.setSize(sf::Vector2f(110, 170));
	this->white_space_2.setOutlineColor(sf::Color::White);
	this->white_space_2.setOutlineThickness(5);
	this->white_space_2.setFillColor(sf::Color::Transparent);

	this->green_space_1.setPosition(this->space1Poz[3].first, this->space1Poz[3].second);
	this->green_space_1.setSize(sf::Vector2f(110, 170));
	this->green_space_1.setOutlineColor(sf::Color::White);
	this->green_space_1.setOutlineThickness(5);
	this->green_space_1.setFillColor(sf::Color::Transparent);

	this->green_space_2.setPosition(this->space2Poz[3].first, this->space2Poz[3].second);
	this->green_space_2.setSize(sf::Vector2f(110, 170));
	this->green_space_2.setOutlineColor(sf::Color::White);
	this->green_space_2.setOutlineThickness(5);
	this->green_space_2.setFillColor(sf::Color::Transparent);

	this->red_space_1.setPosition(this->space1Poz[4].first, this->space1Poz[4].second);
	this->red_space_1.setSize(sf::Vector2f(110, 170));
	this->red_space_1.setOutlineColor(sf::Color::White);
	this->red_space_1.setOutlineThickness(5);
	this->red_space_1.setFillColor(sf::Color::Transparent);

	this->red_space_2.setPosition(this->space2Poz[4].first, this->space2Poz[4].second);
	this->red_space_2.setSize(sf::Vector2f(110, 170));
	this->red_space_2.setOutlineColor(sf::Color::White);
	this->red_space_2.setOutlineThickness(5);
	this->red_space_2.setFillColor(sf::Color::Transparent);

}

void GameState::initDiscardSpaces()
{

	this->discard_yellow_space.setPosition(this->space1Poz[0].first, this->space1Poz[0].second + 200);
	this->discard_yellow_space.setSize(sf::Vector2f(120, 180));
	this->discard_yellow_space.setOutlineColor(sf::Color::White);
	this->discard_yellow_space.setOutlineThickness(5);
	this->discard_yellow_space.setFillColor(sf::Color::Transparent);

	this->discard_blue_space.setPosition(this->space1Poz[1].first - 10, this->space1Poz[1].second + 200);
	this->discard_blue_space.setSize(sf::Vector2f(120, 180));
	this->discard_blue_space.setOutlineColor(sf::Color::White);
	this->discard_blue_space.setOutlineThickness(5);
	this->discard_blue_space.setFillColor(sf::Color::Transparent);

	this->discard_white_space.setPosition(this->space1Poz[2].first - 10, this->space1Poz[2].second + 200);
	this->discard_white_space.setSize(sf::Vector2f(120, 180));
	this->discard_white_space.setOutlineColor(sf::Color::White);
	this->discard_white_space.setOutlineThickness(5);
	this->discard_white_space.setFillColor(sf::Color::Transparent);

	this->discard_green_space.setPosition(this->space1Poz[3].first - 15, this->space1Poz[3].second + 200);
	this->discard_green_space.setSize(sf::Vector2f(120, 180));
	this->discard_green_space.setOutlineColor(sf::Color::White);
	this->discard_green_space.setOutlineThickness(5);
	this->discard_green_space.setFillColor(sf::Color::Transparent);

	this->discard_red_space.setPosition(this->space1Poz[4].first - 15, this->space1Poz[4].second + 200);
	this->discard_red_space.setSize(sf::Vector2f(120, 180));
	this->discard_red_space.setOutlineColor(sf::Color::White);
	this->discard_red_space.setOutlineThickness(5);
	this->discard_red_space.setFillColor(sf::Color::Transparent);
}

void GameState::initPlayersName()
{
	this->player1Name.setFillColor(sf::Color::White);
	this->player1Name.setCharacterSize(80);
	this->player1Name.setPosition(120, 30);
	this->player1Name.setFont(this->font);
	this->player1Name.setOutlineThickness(5);
	this->player1Name.setOutlineColor(sf::Color::Transparent);
	this->player1Name.setString(this->player1.GetName());

	this->player2Name.setFillColor(sf::Color::White);
	this->player2Name.setCharacterSize(80);
	this->player2Name.setPosition(120, 890);
	this->player2Name.setFont(this->font);
	this->player2Name.setOutlineThickness(5);
	this->player2Name.setOutlineColor(sf::Color::Transparent);
	this->player2Name.setString(this->player2.GetName());
	//this->player1Name.setPosition();
}

void GameState::initCardsNumberText()
{
	this->cards_n = std::to_string(this->deck_graphics->getCardsNumber());

	this->cards_number.setFillColor(sf::Color::Yellow);
	this->cards_number.setOutlineColor(sf::Color::Black);
	this->cards_number.setOutlineThickness(5);
	this->cards_number.setCharacterSize(80);
	this->cards_number.setPosition(1710, 286);
	this->cards_number.setFont(this->font);
	this->cards_number.setString(this->cards_n);
}


void GameState::initFonts()
{
	if (!this->font.loadFromFile("../LostCitiesDuelGraphic/Fonts/Dosis-Light.otf"))
	{
		throw("ERROR:Could not load font!");
	}
}

CardGraphics GameState::createCardGraphics(const Card& card, int x, int y)
{
	CardGraphics* aux;
	int number = card.GetNumber();
	Card::Color color = card.GetColor();
	aux = new CardGraphics(x, y, &this->textures["CARD_BACK"], true, 0, card);

	if (number != NULL)
	{
		sf::Text text_aux;
		text_aux.setString(std::to_string(number));
		text_aux.setCharacterSize(44);
		text_aux.setFont(this->font);
		text_aux.setFillColor(sf::Color::Black);
		text_aux.setOutlineColor(sf::Color::White);
		text_aux.setOutlineThickness(5);
		switch (color)
		{
		case Card::Color::None:
			break;

		case Card::Color::Red:

			aux = new CardGraphics(x, y, &this->textures["RED_NORMAL"],
				true, text_aux, card_red, card);
			break;

		case Card::Color::Blue:

			aux = new CardGraphics(x, y, &this->textures["BLUE_NORMAL"],
				true, text_aux, card_blue, card);
			break;

		case Card::Color::Green:

			aux = new CardGraphics(x, y, &this->textures["GREEN_NORMAL"],
				true, text_aux, card_green, card);
			break;

		case Card::Color::Yellow:

			aux = new CardGraphics(x, y, &this->textures["YELLOW_NORMAL"],
				true, text_aux, card_yellow, card);
			break;

		case Card::Color::White:

			aux = new CardGraphics(x, y, &this->textures["WHITE_NORMAL"],
				true, text_aux, card_white, card);
			break;

		default:
			break;
		}
	}
	else
	{
		switch (color)
		{
		case Card::Color::None:
			break;

		case Card::Color::Red:

			aux = new CardGraphics(x, y, &this->textures["RED_INV"],
				true, card_red, card);
			break;

		case Card::Color::Blue:

			aux = new CardGraphics(x, y, &this->textures["BLUE_INV"],
				true, card_blue, card);
			break;

		case Card::Color::Green:

			aux = new CardGraphics(x, y, &this->textures["GREEN_INV"],
				true, card_green, card);
			break;

		case Card::Color::Yellow:

			aux = new CardGraphics(x, y, &this->textures["YELLOW_INV"],
				true, card_yellow, card);
			break;

		case Card::Color::White:

			aux = new CardGraphics(x, y, &this->textures["WHITE_INV"],
				true, card_white, card);
			break;

		default:
			break;
		}
	}
	return *aux;
}
