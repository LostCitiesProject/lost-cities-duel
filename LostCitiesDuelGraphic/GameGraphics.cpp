#include "GameGraphics.h"

//Static functions

//Initializer functions

void GameGraphics::initWindow()
{
	//Creates a SFML window using option from a window.ini file

	std::ifstream ifs("Config/window.ini");
	this->videoModes = sf::VideoMode::getFullscreenModes();

	std::string title = "None";
	sf::VideoMode window_bounds = sf::VideoMode::getDesktopMode();
	bool fullscreen = false;
	unsigned frameRateLimit = 120;
	bool verticalSyncEnabled = false;
	unsigned antialiasing_level = 0;

	if (ifs.is_open())
	{
		std::getline(ifs, title);
		ifs >> window_bounds.width >> window_bounds.height;
		ifs >> fullscreen;
		ifs >> frameRateLimit;
		ifs >> verticalSyncEnabled;
		ifs >> antialiasing_level;
	}

	ifs.close();

	this->windowSettings.antialiasingLevel = antialiasing_level;
	this->fullscreen = fullscreen;
	if (this->fullscreen)
		this->window = new sf::RenderWindow(window_bounds, title, sf::Style::Fullscreen, windowSettings);
	else
		this->window = new sf::RenderWindow(window_bounds, title, sf::Style::Titlebar | sf::Style::Close, windowSettings);

	this->window->setFramerateLimit(frameRateLimit);
	this->window->setVerticalSyncEnabled(verticalSyncEnabled);
}

void GameGraphics::initStates()
{
	this->states.push(new MainMenuState(this->window, &this->states));
	//this->states.push(new GameState(this->window));
}

void GameGraphics::initVariables()
{
	this->window = NULL;
	this->dt = 0.f;
	this->fullscreen = false;
}

//Consstructors/Destructors

GameGraphics::GameGraphics()
{
	this->initWindow();
	this->initStates();
}

GameGraphics::~GameGraphics()
{
	delete this->window;

	while (!this->states.empty())
	{
		delete this->states.top();
		this->states.pop();
	}
}


//Functions

void GameGraphics::endAplication()
{
	std::cout << "Ending Application! \n";
}

void GameGraphics::updateDt()
{
	//Update the dt variable with the time it takes to update and render one frame

	this->dt = this->dtClock.restart().asSeconds();

}

void GameGraphics::updateSfmlEvents()
{
	while (this->window->pollEvent(this->sfEvent))
	{
		if (this->sfEvent.type == sf::Event::Closed)
			this->window->close();
	}

}

void GameGraphics::update()
{
	this->updateSfmlEvents();

	if (!this->states.empty())
	{
		this->states.top()->update(this->dt);

		if (this->states.top()->getQuit())
		{
			this->states.top()->endState();
			delete this->states.top();
			this->states.pop();
		}
	}
	//Aplication end
	else
	{
		this->endAplication();
		this->window->close();
	}

}

void GameGraphics::render()
{
	this->window->clear();

	//render items
	if (!this->states.empty())
		this->states.top()->render();

	this->window->display();
}

void GameGraphics::run()
{
	while (this->window->isOpen())
	{
		this->updateDt();
		this->update();
		this->render();
	}
}
