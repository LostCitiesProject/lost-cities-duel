#pragma once

#include "MainMenuState.h"

class GameGraphics
{
public:
	//Functions

	//Regular
	void endAplication();

	//Update
	void updateDt();
	void updateSfmlEvents();
	void update();
	
	//Render
	void render();

	//Core
	void run();

	//Constructors/Destructors
	GameGraphics();
	virtual ~GameGraphics();

private:
	//Initialization
	void initWindow();
	void initStates();
	void initVariables();

private:
	//Variables
	sf::RenderWindow* window;
	sf::Event sfEvent;
	std::vector<sf::VideoMode> videoModes;
	sf::ContextSettings windowSettings;
	bool fullscreen;

	sf::Clock dtClock;
	float dt;

	std::stack<State*> states;

	std::map<char*, int>supportedKeys;

};

