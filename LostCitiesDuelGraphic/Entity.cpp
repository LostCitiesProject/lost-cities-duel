#include "Entity.h"

void Entity::update(const float& dt, sf::RenderWindow* window)
{
	/*this->mousePosition = sf::Mouse::getPosition();
	if (movement == true)
	{
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && mousePosition.x - 595 >= shape.getPosition().x && mousePosition.x - 595 <= shape.getPosition().x + 50 &&
			mousePosition.y - 300 >= shape.getPosition().y && mousePosition.y - 300 <= shape.getPosition().y + 50)
		{
			this->move(dt, mousePosition.x, mousePosition.y);
			movement = false;
		}
	}
	else
	{
		this->move(dt, mousePosition.x, mousePosition.y);
		if (!sf::Mouse::isButtonPressed(sf::Mouse::Left))
			movement = true;
	}*/
	//sf::Event e;
	this->updateMousePosition(window);
	if (this->move_cond == true)
	{
		if (movement == true)// && /e.MouseButtonPressed)
		{
			if (this->sprite->getGlobalBounds().contains(mousePosView) && sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
			{
				this->setPosition(mousePosView.x - 50, mousePosView.y - 80);
				movement = false;
			}
		}
		else
		{
			this->setPosition(mousePosView.x - 50, mousePosView.y - 80);
			if (!sf::Mouse::isButtonPressed(sf::Mouse::Left))
				movement = true;
		}
	}


}

void Entity::updateMousePosition(sf::RenderWindow* window)
{
	this->mousePosScreen = sf::Mouse::getPosition();
	this->mousePosWindow = sf::Mouse::getPosition(*window);
	this->mousePosView = window->mapPixelToCoords(sf::Mouse::getPosition(*window));
}


void Entity::render(sf::RenderTarget* target)
{
	//target->draw(this->shape);
	if (this->sprite)
	{
		target->draw(*this->sprite);
		if (isText == true)
		target->draw(this->text);
	}
}

void Entity::setPosition(const float x, const float y)
{
	if (this->sprite)
	{
		this->sprite->setPosition(x, y);
		this->setTextPosition(x+40, y+5);
	}
}

void Entity::setTextPosition(const float x, const float y)
{
	this->text.setPosition(x, y);
}

void Entity::resetPosition(const float x, const float y)
{
	this->setPosition(x, y);
}

void Entity::setMove(bool cond)
{
	this->move_cond = cond;
}

//Component functions
void Entity::createSprite(sf::Texture* texture)
{
	this->texture = texture;
	this->sprite = new sf::Sprite(*this->texture);
	this->sprite->setTexture(*this->texture);
}

void Entity::createSprite(sf::Texture* texture, sf::Text numar)
{
	this->texture = texture;
	this->sprite = new sf::Sprite(*this->texture);
	this->sprite->setTexture(*this->texture);
	this->text = numar;
	this->isText = true;
}

bool Entity::getMovement()
{
	return this->movement;
}

bool Entity::getMovementCond()
{
	return this->move_cond;
}


//Constructors/Destructors
Entity::Entity(bool moveCondition):
	move_cond(moveCondition)
{
	//this->shape.setSize(sf::Vector2f(50.f, 50.f));
	//this->shape.setFillColor(sf::Color::White);
	this->initVariable();
}

Entity::~Entity()
{
	delete this->sprite;
}

void Entity::initVariable()
{
	this->sprite = nullptr;
	this->texture = nullptr;
}
