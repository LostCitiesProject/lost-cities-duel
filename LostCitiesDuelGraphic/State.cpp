#include "State.h"

void State::updateScore(const Table& table)
{
	uint16_t numberOfCards;
	int columnScore;
	uint16_t investment;
	uint16_t sizeColumns = 5;
	uint16_t sizeLines = 24;
	int scorePlayerOneRound = 0;
	int scorePlayerTwoRound = 0;
	/*Card::Color color=Card::Color::Blue;
	Card card(color,NULL);*/
	for (uint16_t index2 = 0; index2 < sizeColumns; index2++)
	{
		numberOfCards = 0;
		columnScore = 0;
		investment = 1;
		for (uint16_t index = 0; index < sizeLines / 2; index++)
		{
			if (table[{index, index2}].has_value()) 
			{
				if (table[{index, index2}]->GetNumber() == NULL)
				{
					investment++;
				}
				else
				{
					numberOfCards++;
					columnScore += table[{index, index2}]->GetNumber();
				}
			}
		}
		if (numberOfCards != 0 || investment != 1)
		{
			columnScore -= 20;
			columnScore *= investment;
			scorePlayerOneRound += columnScore;

			if (numberOfCards + investment - 1 >= 8)
			{
				scorePlayerOneRound += 20;
			}
		}
	}
	std::cout << "Player one score for this round is:" << scorePlayerOneRound << '\n';
	if (this->scorePlayer1 != NULL)
		this->scorePlayer1 += scorePlayerOneRound;
	else
		this->scorePlayer1 = scorePlayerOneRound;

	for (uint16_t index2 = 0; index2 < sizeColumns; index2++)
	{
		numberOfCards = 0;
		columnScore = 0;
		investment = 1;
		for (uint16_t index = sizeLines/2; index < sizeLines; index++)
		{
			if (table[{index, index2}].has_value()) {
				if (table[{index, index2}]->GetNumber() == NULL)
				{
					investment++;
				}
				else
				{
					numberOfCards++;
					columnScore += table[{index, index2}]->GetNumber();
				}
			}
		}
		if (numberOfCards != 0 || investment != 1)
		{
			columnScore -= 20;
			columnScore *= investment;
			scorePlayerTwoRound += columnScore;

			if (numberOfCards + investment - 1 >= 8)
			{
				scorePlayerTwoRound += 20;
			}
		}
	}
	std::cout << "Player two score for this round is:" << scorePlayerTwoRound << '\n';
	if (this->scorePlayer2 != NULL)
		this->scorePlayer2 += scorePlayerTwoRound;
	else
		this->scorePlayer2 = scorePlayerTwoRound;
}

void State::endState()
{
	this->quit = true;
}

void State::pauseState()
{
	this->paused = true;
}

void State::unpauseState()
{
	this->paused = false;
}

void State::updateMousePosition()
{
	this->mousePosScreen = sf::Mouse::getPosition();
	this->mousePosWindow = sf::Mouse::getPosition(*this->window);
	this->mousePosView = this->window->mapPixelToCoords(sf::Mouse::getPosition(*this->window));
}

void State::updateKeyTime(const float& dt)
{
	if (this->keyTime < this->keyTimeMax)
		this->keyTime += 100.f * dt;
}

const bool& State::getQuit() const
{
	return this->quit;
}

const bool State::getKeyTime()
{
	if (this->keyTime >= this->keyTimeMax)
	{
		this->keyTime = 0.f;
		return true;
	}
	return false;
}

State::State(sf::RenderWindow* window, std::stack<State*>* states)
{
	this->window = window;
	this->states = states;
	this->quit = false;
	this->paused = false;
	this->keyTime = 0.f;
	this->keyTimeMax = 10.f;
}

State::~State()
{

}
