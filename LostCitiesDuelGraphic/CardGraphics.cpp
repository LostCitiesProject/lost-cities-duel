#include "CardGraphics.h"

//Initializer
void CardGraphics::initVariables()
{
	this->shape.setPosition(this->sprite->getPosition().x,this->sprite->getPosition().y);
	this->shape.setSize(sf::Vector2f(50, 50));
	std::string text("Text");
	this->font = font;
	this->text.setFont(*this->font);
	this->text.setString(text);
	this->text.setFillColor(sf::Color::White);
	this->text.setCharacterSize(24);
	this->text.setPosition(
		this->shape.getPosition().x + (this->shape.getGlobalBounds().width / 2.f) - this->text.getGlobalBounds().width / 2.f,
		this->shape.getPosition().y + (this->shape.getGlobalBounds().height / 2.f) - this->text.getGlobalBounds().height / 2.f
	);

	this->shape.setFillColor(sf::Color::White);
	//sf::RenderTarget* target;
	//target=this->w
	//target->draw(shape);
}

void CardGraphics::initComponents()
{
	//this->createSprite()
}

void CardGraphics::reset()
{
	this->resetPosition(this->initX, this->initY);
}

sf::Texture CardGraphics::getTexture()
{
	return *this->texture;
}

sf::Vector2f CardGraphics::getCoord()
{
	return sf::Vector2f(this->sprite->getPosition());
}

uint16_t CardGraphics::getCardType()
{
	return this->m_card_type;
}

Card CardGraphics::getCard()
{
	return this->card;
}


//Constructors/Destructors
CardGraphics::CardGraphics(float x, float y, sf::Texture* texture, bool moveCondition, uint16_t card_type, Card card) :
	Entity::Entity(moveCondition)
{
	this->initX = x;
	this->initY = y;
	this->m_card_type = card_type;
	this->card = card;

	this->initComponents();
	this->createSprite(texture);
	this->setPosition(x, y);
	this->initVariables();
}

CardGraphics::CardGraphics(float x, float y, sf::Texture* texture, bool moveCondition, sf::Text numar, uint16_t card_type, Card card) :
	Entity::Entity(moveCondition)
{
	this->initX = x;
	this->initY = y;
	this->m_card_type = card_type;
	this->card = card;

	this->initComponents();
	this->createSprite(texture, numar);
	this->setPosition(x, y);
	this->initVariables();
}

CardGraphics::~CardGraphics()
{
}
