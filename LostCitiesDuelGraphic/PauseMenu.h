#pragma once
#include "CardGraphics.h"
#include "TableGraphics.h"
#include"Button.h"
#include<map>

class PauseMenu
{

public:

	std::map<std::string, Button*>& getButtons();

	PauseMenu(sf::RenderWindow& window,sf::Font& font);
	virtual ~PauseMenu();
	void update(const sf::Vector2f& mousePos);
	void render(sf::RenderTarget * target);
	void addButton(const std::string key, float y, const std::string text);
	const bool isButtonPressed(const std::string key);
	
private:
	sf::Font& m_font;
	sf::Text MenuText;

	sf::RectangleShape background;
	sf::RectangleShape container;

	std::map<std::string, Button*> buttons;
};

