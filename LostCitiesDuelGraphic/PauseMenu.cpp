#include "PauseMenu.h"

std::map<std::string, Button*>& PauseMenu::getButtons()
{
	return this->buttons;
}

PauseMenu::PauseMenu(sf::RenderWindow& window,sf::Font& font):m_font(font)
{
	this->background.setSize(
		sf::Vector2f
			(static_cast<float>(window.getSize().x),
			static_cast<float> (window.getSize().y)));

	this->background.setFillColor(sf::Color(20, 20, 20, 100));

	this->container.setSize(
		sf::Vector2f
			(static_cast<float>(window.getSize().x)/4.f,
			static_cast<float> (window.getSize().y)-100.f)
	);

	this->container.setFillColor(sf::Color(20, 20, 20, 200));
	this->container.setPosition(
		static_cast<float>(window.getSize().x) / 2.f - this->container.getSize().x / 2.f,
		30.f
	);

	this->MenuText.setFont(font);
	this->MenuText.setFillColor(sf::Color(255, 255, 255, 200));
	this->MenuText.setCharacterSize(60);
	this->MenuText.setString("PAUSED");
	this->MenuText.setPosition(
		this->container.getPosition().x + this->container.getSize().x/2.f - this->MenuText.getGlobalBounds().width/2.f,
		this->container.getPosition().y+40.f);


}

PauseMenu::~PauseMenu()
{
	for (auto it = this->buttons.begin(); it != buttons.end(); ++it)
		delete it->second;
}

void PauseMenu::update(const sf::Vector2f& mousePos)
{
	for (auto& it : this->buttons)
	{
		it.second->update(mousePos);
	}
}

void PauseMenu::render(sf::RenderTarget* target)
{
	target->draw(this->background);
	target->draw(this->container);

	for (auto& it : this->buttons)
	{
		it.second->render(target);
	}
	target->draw(this->MenuText);
}

void PauseMenu::addButton(const std::string key,float y, const std::string text)
{
	float width = 250.f;
	float height = 50.f;
	float x = this->container.getPosition().x + this->container.getSize().x / 2.f - width / 2.f;

	this->buttons[key] = new Button(x, y, width, height,
		&this->m_font, text,
		sf::Color(100, 100, 100, 200), sf::Color(150, 150, 150, 255), sf::Color(20, 20, 20, 200));
}

const bool PauseMenu::isButtonPressed(const std::string key)
{
	return this->buttons[key]->isPressed();
}
