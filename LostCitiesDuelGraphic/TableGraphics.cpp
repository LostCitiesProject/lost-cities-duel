#include "TableGraphics.h"

TableGraphics::TableGraphics(float x, float y, sf::Texture* texture, bool moveCondition):
	Entity::Entity(moveCondition)
{
	//this->initComponents();
	//this->initVariables();
	this->createSprite(texture);
	this->setPosition(x, y);
}

TableGraphics::~TableGraphics()
{
}

