#pragma once

#include <vector>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <stack>
#include <map>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>


class Entity
{
public:
	//Functions
	virtual void updateMousePosition(sf::RenderWindow* window);
	virtual void update(const float& dt, sf::RenderWindow* window);

	virtual void render(sf::RenderTarget* target);

	virtual void setPosition(const float x, const float y);
	virtual void setTextPosition(const float x, const float y);
	virtual void resetPosition(const float x, const float y);
	virtual void setMove(bool cond);

	//ComponentFuncitons
	void createSprite(sf::Texture* texture);
	void createSprite(sf::Texture* texture, sf::Text numar);

	virtual bool getMovement();
	virtual bool getMovementCond();

public:
	//Constructors/Destructors
	Entity(bool moveCondition);
	virtual ~Entity();

private:
	void initVariable();

protected:
	sf::Sprite* sprite;
	sf::Texture* texture;
	sf::Text text;

	//sf::RenderWindow* window;
	bool move_cond;
	bool isText = false;
	bool movement = true;

	sf::Vector2i mousePosScreen;
	sf::Vector2i mousePosWindow;
	sf::Vector2f mousePosView;
	/*sf::RectangleShape shape;
	sf::Vector2i mousePosition;*/

};

