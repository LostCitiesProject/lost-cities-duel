#pragma once
#include"State.h"
#include"Button.h"

class SettingsState:public State
{
public:
	SettingsState(sf::RenderWindow* window, std::stack<State*>* states);
	virtual ~SettingsState();

	void update(const float& dt);
	void updateInput(const float& dt);
	void updateButtons();
	void renderButtons(sf::RenderTarget* target = nullptr);
	void render(sf::RenderTarget* target = nullptr);

private:

	void initVariables();
	void initBackground();
	void initFonts();
	void initButtons();

private:
	sf::RectangleShape background;
	sf::Texture backgroundTexture;
	sf::Font font;

	std::map<std::string, Button*> buttons;
};

