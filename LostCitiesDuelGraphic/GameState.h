#pragma once

#include <locale>

#include "State.h"
#include "PauseMenu.h"
#include "DeckGraphics.h"
#include "FinalState.h"

#include "..\Lost Cities Duel\Player.h"

class GameState :
	public State
{
public:
	//Functions
	void render(sf::RenderTarget* target = nullptr);
	void renderPlayer1(sf::RenderTarget* target = nullptr);
	void renderPlayer2(sf::RenderTarget* target = nullptr);
	void renderSpaces(sf::RenderTarget* target = nullptr);
	void renderDiscardSpaces(sf::RenderTarget* target = nullptr);
	void renderDiscardCards(sf::RenderTarget* target = nullptr);
	void renderPlayersName(sf::RenderTarget* target = nullptr);
	void renderCardsNumberText(sf::RenderTarget* target = nullptr);

	void update(const float& dt);
	void updateCardsNumberText(const float& dt);
	void updatePlayersName(const float& dt);
	void updatePlayer1(const float& dt);
	void updatePlayer2(const float& dt);
	void updateInput(const float& dt);
	void updatePauseMenuButtons();

	void initFonts();
	void initTextures();
	void initCards();
	void initTable();
	void initDeck();
	void initBackground();
	void initPauseMenu();
	void initPlayers();
	void initSpaces();
	void initDiscardSpaces();
	void initPlayersName();
	void initCardsNumberText();

	CardGraphics createCardGraphics(const Card& card, int x, int y);

public:
	GameState(sf::RenderWindow* window, std::stack<State*>* states,std::string p1Name,std::string p2Name, std::string p1Age,
		std::string p2Age);
	virtual ~GameState();

private:
	sf::RectangleShape background;

	//player one part
	sf::RectangleShape yellow_space_1;
	sf::RectangleShape blue_space_1;
	sf::RectangleShape white_space_1;
	sf::RectangleShape green_space_1;
	sf::RectangleShape red_space_1;

	//player two part
	sf::RectangleShape yellow_space_2;
	sf::RectangleShape blue_space_2;
	sf::RectangleShape white_space_2;
	sf::RectangleShape green_space_2;
	sf::RectangleShape red_space_2;

	//player 1 spaces vectors
	std::vector <CardGraphics*> yellow_1;
	std::vector <CardGraphics*> blue_1;
	std::vector <CardGraphics*> red_1;
	std::vector <CardGraphics*> green_1;
	std::vector <CardGraphics*> white_1;

	//player 2 spaces vectors
	std::vector <CardGraphics*> white_2;
	std::vector <CardGraphics*> green_2;
	std::vector <CardGraphics*> red_2;
	std::vector <CardGraphics*> blue_2;
	std::vector <CardGraphics*> yellow_2;

	//discarded cards graphics
	std::vector <CardGraphics*> white_discarded;
	std::vector <CardGraphics*> green_discarded;
	std::vector <CardGraphics*> red_discarded;
	std::vector <CardGraphics*> blue_discarded;
	std::vector <CardGraphics*> yellow_discarded;

	//discarded spaces
	sf::RectangleShape discard_yellow_space;
	sf::RectangleShape discard_blue_space;
	sf::RectangleShape discard_white_space;
	sf::RectangleShape discard_green_space;
	sf::RectangleShape discard_red_space;

	std::vector<std::pair<int, int>> space1Poz;
	std::vector<std::pair<int, int>> space2Poz;

	std::vector<std::pair<int, int>> placeMarker;
	std::vector<std::pair<int, int>> placeMarker2;

	sf::Texture backgroundTexture;
	sf::Font font;

	std::vector<CardGraphics*> cards;
	std::vector<CardGraphics*> player1cards;
	std::vector<CardGraphics*> player2cards;
	TableGraphics* table;
	PauseMenu* m_Menu;

	int poz = -1;
	int poz2 = -1;
	bool turn = true;


	uint16_t age;
	Player player1;

	uint16_t age1;
	Player player2;

	Table board;

	uint16_t scorePlayerOne = 0;
	uint16_t scorePlayerTwo = 0;
	uint16_t numberOfRounds;

	std::vector<std::pair<int, int>> player1HandPoz;
	std::vector<std::pair<int, int>> player2HandPoz;

	const uint16_t card_back = 0;
	const uint16_t card_yellow = 1;
	const uint16_t card_blue = 2;
	const uint16_t card_white = 3;
	const uint16_t card_red = 4;
	const uint16_t card_green = 5;

	DeckGraphics* deck_graphics;

	sf::Text player1Name;
	sf::Text player2Name;

	std::string saved_player1Name;
	std::string saved_player2Name;

	std::string saved_player1Age;
	std::string saved_player2Age;

	sf::Text cards_number;
	std::string cards_n;
};

