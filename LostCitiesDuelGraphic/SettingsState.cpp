#include "SettingsState.h"

SettingsState::SettingsState(sf::RenderWindow* window, std::stack<State*>* states):State(window,states)
{
	this->initVariables();
	this->initBackground();
	this->initFonts();
	this->initButtons();
}

SettingsState::~SettingsState()
{
	for (auto it = this->buttons.begin(); it != buttons.end(); ++it)
		delete it->second;
}

void SettingsState::initVariables()
{
}

void SettingsState::initBackground()
{
	this->background.setSize(
		sf::Vector2f
		(
			static_cast<float>(this->window->getSize().x),
			static_cast<float>(this->window->getSize().y)
		)
	);
	if (!this->backgroundTexture.loadFromFile("Resources/Images/Background/bg1.png"))
	{
		throw "ERROR:MAIN_MENU_STATE::FAILED_TO_LOAD_BACKGROUND_TEXTURE";
	}
	this->background.setTexture(&this->backgroundTexture);
}

void SettingsState::initFonts()
{
	if (!this->font.loadFromFile("Fonts/Dosis-Light.otf"))
	{
		throw("ERROR:Could not load font!");
	}
}

void SettingsState::initButtons()
{
	this->buttons["EXIT_STATE"] = new Button(200, 824, 250, 75,
		&this->font, "Quit",
		sf::Color(100, 100, 100, 200), sf::Color(150, 150, 150, 255), sf::Color(20, 20, 20, 200));
}


void SettingsState::update(const float& dt)
{
	this->updateMousePosition();
	this->updateInput(dt);

	this->updateButtons();
}

void SettingsState::updateInput(const float& dt)
{

}

void SettingsState::updateButtons()
{
	/*Updates all the buttons in the state and handle their functionality*/
	for (auto& it : this->buttons)
	{
		it.second->update(this->mousePosView);
	}


	//Quit
	if (this->buttons["EXIT_STATE"]->isPressed())
	{
		this->endState();
	}
}

void SettingsState::render(sf::RenderTarget* target)
{
	if (!target)
		target = this->window;

	target->draw(this->background);

	this->renderButtons(target);

}

void SettingsState::renderButtons(sf::RenderTarget* target)
{
	for (auto& it : this->buttons)
	{
		it.second->render(target);;
	}
}