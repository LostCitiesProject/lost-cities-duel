#include "InitState.h"

InitState::InitState(sf::RenderWindow* window, std::stack<State*>* states) :
	State(window, states)
{

	this->initBackground();
	this->initFonts();
	this->initNameWindow();
}

InitState::~InitState()
{
}



void InitState::update(const float& dt)
{
	this->updateInput(dt);
	this->updateText(dt);
}

void InitState::updateText(const float& dt)
{


	if (this->background_t.getGlobalBounds().contains(mousePosView) && sf::Mouse::isButtonPressed(sf::Mouse::Left) && select != 0)
	{
		std::cout << "s1";
		this->select = 0;
	}
	if (this->background_t_2.getGlobalBounds().contains(mousePosView) && sf::Mouse::isButtonPressed(sf::Mouse::Left) && select != 1)
	{

		std::cout << "s2";
		this->select = 1;
	}

	if (this->background_rounds.getGlobalBounds().contains(mousePosView) && sf::Mouse::isButtonPressed(sf::Mouse::Left) && select != 2)
	{
		std::cout << "s3";
		this->select = 2;
	}

	if (this->background_t_2_age.getGlobalBounds().contains(mousePosView) && sf::Mouse::isButtonPressed(sf::Mouse::Left) && select != 3)
	{

		std::cout << "s4";
		this->select = 3;
	}
	if (this->background_t_age.getGlobalBounds().contains(mousePosView) && sf::Mouse::isButtonPressed(sf::Mouse::Left) && select != 4)
	{

		std::cout << "s5";
		this->select = 4;
	}

	if (select == 0)
	{
		//sf::Event e;
		std::string aux;

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			if (count_k[0] == 0)
			{
				aux += "a";
				count_k[0] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::B))
		{
			if (count_k[1] == 0)
			{
				aux += "b";
				count_k[1] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::C))
		{
			if (count_k[3] == 0)
			{
				aux += "c";
				count_k[3] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			if (count_k[4] == 0)
			{
				aux += "d";
				count_k[4] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::E))
		{
			if (count_k[5] == 0)
			{
				aux += "e";
				count_k[5] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::F))
		{
			if (count_k[6] == 0)
			{
				aux += "f";
				count_k[6] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::G))
		{
			if (count_k[7] == 0)
			{
				aux += "g";
				count_k[7] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::H))
		{
			if (count_k[8] == 0)
			{
				aux += "h";
				count_k[8] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::K))
		{
			if (count_k[9] == 0)
			{
				aux += "k";
				count_k[9] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::L))
		{
			if (count_k[10] == 0)
			{
				aux += "l";
				count_k[10] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::M))
		{
			if (count_k[11] == 0)
			{
				aux += "m";
				count_k[11] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::N))
		{
			if (count_k[12] == 0)
			{
				aux += "n";
				count_k[12] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::O))
		{
			if (count_k[14] == 0)
			{
				aux += "o";
				count_k[14] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::P))
		{
			if (count_k[15] == 0)
			{
				aux += "p";
				count_k[15] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
		{
			if (count_k[16] == 0)
			{
				aux += "q";
				count_k[16] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{
			if (count_k[17] == 0)
			{
				aux += "r";
				count_k[17] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			if (count_k[18] == 0)
			{
				aux += "s";
				count_k[18] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::T))
		{
			if (count_k[19] == 0)
			{
				aux += "t";
				count_k[19] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::X))
		{
			if (count_k[20] == 0)
			{
				aux += "x";
				count_k[20] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Y))
		{
			if (count_k[21] == 0)
			{
				aux += "y";
				count_k[21] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			if (count_k[22] == 0)
			{
				aux += "w";
				count_k[22] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
		{
			if (count_k[23] == 0)
			{
				aux += "z";
				count_k[23] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::I))
		{
			if (count_k[24] == 0)
			{
				aux += "i";
				count_k[24] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::J))
		{
			if (count_k[25] == 0)
			{
				aux += "j";
				count_k[25] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::U))
		{
			if (count_k[26] == 0)
			{
				aux += "u";
				count_k[26] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::V))
		{
			if (count_k[13] == 0)
			{
				aux += "v";
				count_k[13] = 1;
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace))
		{
			if (count_k[2] == 0)
			{
				std::string rezid;
				if (t_enter.getString().getSize() != 0)
				{
					rezid = t_enter.getString();
					rezid.pop_back();
					this->t_enter.setString(rezid);
					count_k[2] = 1;
				}
			}
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			count_k[0] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::B))
		{
			count_k[1] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace))
		{
			count_k[2] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::C))
		{
			count_k[3] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			count_k[4] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::E))
		{
			count_k[5] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::F))
		{
			count_k[6] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::G))
		{
			count_k[7] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::H))
		{
			count_k[8] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::K))
		{
			count_k[9] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::L))
		{
			count_k[10] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::M))
		{
			count_k[11] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::N))
		{
			count_k[12] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::V))
		{
			count_k[13] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::O))
		{
			count_k[14] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::P))
		{
			count_k[15] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
		{
			count_k[16] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{
			count_k[17] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			count_k[18] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::T))
		{
			count_k[19] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::X))
		{
			count_k[20] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Y))
		{
			count_k[21] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			count_k[22] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
		{
			count_k[23] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::I))
		{
			count_k[24] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::J))
		{
			count_k[25] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::U))
		{
			count_k[26] = 0;
		}

		if (aux.size() != 0)
		{
			this->t_enter.setString(this->t_enter.getString() + aux);
		}


		if (!this->background_t.getGlobalBounds().contains(mousePosView) && sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			std::cout << "~s1";
			this->select = -1;
		}


	}
	if (select == 1)
	{

		//sf::Event e;
		std::string aux;

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			if (count_k[0] == 0)
			{
				aux += "a";
				count_k[0] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::B))
		{
			if (count_k[1] == 0)
			{
				aux += "b";
				count_k[1] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::C))
		{
			if (count_k[3] == 0)
			{
				aux += "c";
				count_k[3] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			if (count_k[4] == 0)
			{
				aux += "d";
				count_k[4] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::E))
		{
			if (count_k[5] == 0)
			{
				aux += "e";
				count_k[5] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::F))
		{
			if (count_k[6] == 0)
			{
				aux += "f";
				count_k[6] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::G))
		{
			if (count_k[7] == 0)
			{
				aux += "g";
				count_k[7] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::H))
		{
			if (count_k[8] == 0)
			{
				aux += "h";
				count_k[8] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::K))
		{
			if (count_k[9] == 0)
			{
				aux += "k";
				count_k[9] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::L))
		{
			if (count_k[10] == 0)
			{
				aux += "l";
				count_k[10] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::M))
		{
			if (count_k[11] == 0)
			{
				aux += "m";
				count_k[11] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::N))
		{
			if (count_k[12] == 0)
			{
				aux += "n";
				count_k[12] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::O))
		{
			if (count_k[14] == 0)
			{
				aux += "o";
				count_k[14] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::P))
		{
			if (count_k[15] == 0)
			{
				aux += "p";
				count_k[15] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
		{
			if (count_k[16] == 0)
			{
				aux += "q";
				count_k[16] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{
			if (count_k[17] == 0)
			{
				aux += "r";
				count_k[17] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			if (count_k[18] == 0)
			{
				aux += "s";
				count_k[18] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::T))
		{
			if (count_k[19] == 0)
			{
				aux += "t";
				count_k[19] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::X))
		{
			if (count_k[20] == 0)
			{
				aux += "x";
				count_k[20] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Y))
		{
			if (count_k[21] == 0)
			{
				aux += "y";
				count_k[21] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			if (count_k[22] == 0)
			{
				aux += "w";
				count_k[22] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
		{
			if (count_k[23] == 0)
			{
				aux += "z";
				count_k[23] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::I))
		{
			if (count_k[24] == 0)
			{
				aux += "i";
				count_k[24] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::J))
		{
			if (count_k[25] == 0)
			{
				aux += "j";
				count_k[25] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::U))
		{
			if (count_k[26] == 0)
			{
				aux += "u";
				count_k[26] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::V))
		{
			if (count_k[13] == 0)
			{
				aux += "v";
				count_k[13] = 1;
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace))
		{
			if (count_k[2] == 0)
			{
				std::string rezid;
				if (t_2_enter.getString().getSize() != 0)
				{
					rezid = t_2_enter.getString();
					rezid.pop_back();
					this->t_2_enter.setString(rezid);
					count_k[2] = 1;
				}
			}
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			count_k[0] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::B))
		{
			count_k[1] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace))
		{
			count_k[2] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::C))
		{
			count_k[3] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			count_k[4] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::E))
		{
			count_k[5] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::F))
		{
			count_k[6] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::G))
		{
			count_k[7] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::H))
		{
			count_k[8] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::K))
		{
			count_k[9] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::L))
		{
			count_k[10] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::M))
		{
			count_k[11] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::N))
		{
			count_k[12] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::V))
		{
			count_k[13] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::O))
		{
			count_k[14] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::P))
		{
			count_k[15] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
		{
			count_k[16] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{
			count_k[17] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			count_k[18] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::T))
		{
			count_k[19] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::X))
		{
			count_k[20] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Y))
		{
			count_k[21] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			count_k[22] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
		{
			count_k[23] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::I))
		{
			count_k[24] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::J))
		{
			count_k[25] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::U))
		{
			count_k[26] = 0;
		}

		if (aux.size() != 0)
		{
			this->t_2_enter.setString(this->t_2_enter.getString() + aux);
		}


		if (!this->background_t_2.getGlobalBounds().contains(mousePosView) && sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			std::cout << "~s2";
			this->select = -1;
		}


	}
	if (select == 2) {
		std::string aux;

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num0))
		{
			if (count_k_age[0] == 0)
			{
				aux += "0";
				count_k_age[0] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
		{
			if (count_k_age[1] == 0)
			{
				aux += "1";
				count_k_age[1] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
		{
			if (count_k_age[2] == 0)
			{
				aux += "2";
				count_k_age[2] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
		{
			if (count_k_age[3] == 0)
			{
				aux += "3";
				count_k_age[3] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num4))
		{
			if (count_k_age[4] == 0)
			{
				aux += "4";
				count_k_age[4] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num5))
		{
			if (count_k_age[5] == 0)
			{
				aux += "5";
				count_k_age[5] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num6))
		{
			if (count_k_age[6] == 0)
			{
				aux += "6";
				count_k_age[6] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num7))
		{
			if (count_k_age[7] == 0)
			{
				aux += "7";
				count_k_age[7] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num8))
		{
			if (count_k_age[8] == 0)
			{
				aux += "8";
				count_k_age[8] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num9))
		{
			if (count_k_age[9] == 0)
			{
				aux += "9";
				count_k_age[9] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace))
		{
			if (count_k_age[10] == 0)
			{
				std::string rezid;
				if (t_rounds_enter.getString().getSize() != 0)
				{
					rezid = t_rounds_enter.getString();
					rezid.pop_back();
					this->t_rounds_enter.setString(rezid);
					count_k_age[10] = 1;
				}
			}
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace))
		{
			count_k_age[10] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num0))
		{
			count_k_age[0] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
		{
			count_k_age[1] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
		{
			count_k_age[2] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
		{
			count_k_age[3] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num4))
		{
			count_k_age[4] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num5))
		{
			count_k_age[5] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num6))
		{
			count_k_age[6] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num7))
		{
			count_k_age[7] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num8))
		{
			count_k_age[8] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num9))
		{
			count_k_age[9] = 0;
		}


		if (aux.size() != 0)
		{
			this->t_rounds_enter.setString(this->t_rounds_enter.getString() + aux);
		}


		if (!this->background_rounds.getGlobalBounds().contains(mousePosView) && sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			std::cout << "~s3";
			this->select = -1;
		}
	}
	if (select == 3)
	{
		std::string aux;

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num0))
		{
			if (count_k_age[0] == 0)
			{
				aux += "0";
				count_k_age[0] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
		{
			if (count_k_age[1] == 0)
			{
				aux += "1";
				count_k_age[1] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
		{
			if (count_k_age[2] == 0)
			{
				aux += "2";
				count_k_age[2] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
		{
			if (count_k_age[3] == 0)
			{
				aux += "3";
				count_k_age[3] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num4))
		{
			if (count_k_age[4] == 0)
			{
				aux += "4";
				count_k_age[4] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num5))
		{
			if (count_k_age[5] == 0)
			{
				aux += "5";
				count_k_age[5] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num6))
		{
			if (count_k_age[6] == 0)
			{
				aux += "6";
				count_k_age[6] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num7))
		{
			if (count_k_age[7] == 0)
			{
				aux += "7";
				count_k_age[7] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num8))
		{
			if (count_k_age[8] == 0)
			{
				aux += "8";
				count_k_age[8] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num9))
		{
			if (count_k_age[9] == 0)
			{
				aux += "9";
				count_k_age[9] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace))
		{
			if (count_k_age[10] == 0)
			{
				std::string rezid;
				if (t_2_age_enter.getString().getSize() != 0)
				{
					rezid = t_2_age_enter.getString();
					rezid.pop_back();
					this->t_2_age_enter.setString(rezid);
					count_k_age[10] = 1;
				}
			}
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace))
		{
			count_k_age[10] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num0))
		{
			count_k_age[0] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
		{
			count_k_age[1] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
		{
			count_k_age[2] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
		{
			count_k_age[3] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num4))
		{
			count_k_age[4] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num5))
		{
			count_k_age[5] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num6))
		{
			count_k_age[6] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num7))
		{
			count_k_age[7] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num8))
		{
			count_k_age[8] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num9))
		{
			count_k_age[9] = 0;
		}


		if (aux.size() != 0)
		{
			this->t_2_age_enter.setString(this->t_2_age_enter.getString() + aux);
		}


		if (!this->background_t_2_age.getGlobalBounds().contains(mousePosView) && sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			std::cout << "~s4";
			this->select = -1;
		}
	}
	if (select == 4)
	{
		std::string aux;

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num0))
		{
			if (count_k_age[0] == 0)
			{
				aux += "0";
				count_k_age[0] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
		{
			if (count_k_age[1] == 0)
			{
				aux += "1";
				count_k_age[1] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
		{
			if (count_k_age[2] == 0)
			{
				aux += "2";
				count_k_age[2] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
		{
			if (count_k_age[3] == 0)
			{
				aux += "3";
				count_k_age[3] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num4))
		{
			if (count_k_age[4] == 0)
			{
				aux += "4";
				count_k_age[4] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num5))
		{
			if (count_k_age[5] == 0)
			{
				aux += "5";
				count_k_age[5] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num6))
		{
			if (count_k_age[6] == 0)
			{
				aux += "6";
				count_k_age[6] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num7))
		{
			if (count_k_age[7] == 0)
			{
				aux += "7";
				count_k_age[7] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num8))
		{
			if (count_k_age[8] == 0)
			{
				aux += "8";
				count_k_age[8] = 1;
			}
		}

		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num9))
		{
			if (count_k_age[9] == 0)
			{
				aux += "9";
				count_k_age[9] = 1;
			}
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace))
		{
			if (count_k_age[10] == 0)
			{
				std::string rezid;
				if (t_age_enter.getString().getSize() != 0)
				{
					rezid = t_age_enter.getString();
					rezid.pop_back();
					this->t_age_enter.setString(rezid);
					count_k_age[10] = 1;
				}
			}
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace))
		{
			count_k_age[10] = 0;
		}
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num0))
		{
			count_k_age[0] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
		{
			count_k_age[1] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
		{
			count_k_age[2] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
		{
			count_k_age[3] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num4))
		{
			count_k_age[4] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num5))
		{
			count_k_age[5] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num6))
		{
			count_k_age[6] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num7))
		{
			count_k_age[7] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num8))
		{
			count_k_age[8] = 0;
		}

		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Num9))
		{
			count_k_age[9] = 0;
		}


		if (aux.size() != 0)
		{
			this->t_age_enter.setString(this->t_age_enter.getString() + aux);
		}


		if (!this->background_t_age.getGlobalBounds().contains(mousePosView) && sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			std::cout << "~s4";
			this->select = -1;
		}
	}


	this->send->update(mousePosView);

	if (this->send->isPressed())
	{
		std::string rezid=this->t_rounds_enter.getString();
		int cond = std::stoi(rezid);
		this->number_of_rounds = cond;
		for (int i = 0; i < cond; i++)
		{
			this->states->push(new GameState(this->window, this->states,
				this->t_enter.getString(), this->t_2_enter.getString(), this->t_age_enter.getString(), this->t_2_age_enter.getString()));
		}

	}
}

void InitState::updateInput(const float& dt)
{
	this->updateMousePosition();

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) && this->getKeyTime())
	{
		//
	}
}

void InitState::render(sf::RenderTarget* target)
{
	if (target == nullptr)
	{
		target = this->window;
	}

	target->draw(this->background);

	target->draw(this->t);
	target->draw(this->t_2);

	target->draw(this->background_t);
	target->draw(this->background_t_2);

	target->draw(this->t_enter);
	target->draw(this->t_2_enter);

	target->draw(this->t_age);
	target->draw(this->t_2_age);

	target->draw(this->background_t_age);
	target->draw(this->background_t_2_age);

	target->draw(this->t_age_enter);
	target->draw(this->t_2_age_enter);


	target->draw(this->background_rounds);
	target->draw(this->t_rounds);
	target->draw(this->t_rounds_enter);

	this->send->render(target);
}

void InitState::initNameWindow()
{
	this->t.setString("Player one name:");
	this->t_2.setString("Player two name:");

	this->t.setFont(this->font);
	this->t_2.setFont(this->font);

	this->t.setFillColor(sf::Color::Black);
	this->t_2.setFillColor(sf::Color::Black);

	this->t.setCharacterSize(44);
	this->t_2.setCharacterSize(44);

	this->t.setPosition(450, 200);
	this->t_2.setPosition(1150, 200);

	this->background_t.setFillColor(sf::Color::White);
	this->background_t_2.setFillColor(sf::Color::White);

	this->background_t.setPosition(400, 300);
	this->background_t_2.setPosition(1100, 300);

	this->background_t.setSize(sf::Vector2f(400, 100));
	this->background_t_2.setSize(sf::Vector2f(400, 100));

	this->t_enter.setFont(this->font);
	this->t_enter.setFillColor(sf::Color::Black);
	this->t_enter.setCharacterSize(24);
	this->t_enter.setPosition(
		this->background_t.getPosition().x + (this->background_t.getGlobalBounds().width / 2.f) - this->t_enter.getGlobalBounds().width / 2.f - 100,
		this->background_t.getPosition().y + (this->background_t.getGlobalBounds().height / 2.f) - this->t_enter.getGlobalBounds().height / 2.f
	);

	this->t_2_enter.setFont(this->font);
	this->t_2_enter.setFillColor(sf::Color::Black);
	this->t_2_enter.setCharacterSize(24);
	this->t_2_enter.setPosition(
		this->background_t_2.getPosition().x + (this->background_t_2.getGlobalBounds().width / 2.f) - this->t_2_enter.getGlobalBounds().width / 2.f - 100,
		this->background_t_2.getPosition().y + (this->background_t_2.getGlobalBounds().height / 2.f) - this->t_2_enter.getGlobalBounds().height / 2.f
	);

	for (int index = 0; index < 28; index++)
		this->count_k.push_back(0);

	for (int index = 0; index < 11; index++)
		this->count_k_age.push_back(0);

	this->t_age.setString("Player one age:");
	this->t_2_age.setString("Player two age:");

	this->t_age.setFont(this->font);
	this->t_2_age.setFont(this->font);

	this->t_age.setFillColor(sf::Color::Black);
	this->t_2_age.setFillColor(sf::Color::Black);

	this->t_age.setCharacterSize(44);
	this->t_2_age.setCharacterSize(44);

	this->t_age.setPosition(450, 500);
	this->t_2_age.setPosition(1150, 500);

	this->background_t_age.setFillColor(sf::Color::White);
	this->background_t_2_age.setFillColor(sf::Color::White);

	this->background_t_age.setPosition(400, 600);
	this->background_t_2_age.setPosition(1100, 600);

	this->background_t_age.setSize(sf::Vector2f(400, 100));
	this->background_t_2_age.setSize(sf::Vector2f(400, 100));

	this->t_age_enter.setFont(this->font);
	this->t_age_enter.setFillColor(sf::Color::Black);
	this->t_age_enter.setCharacterSize(24);
	this->t_age_enter.setPosition(
		this->background_t_age.getPosition().x + (this->background_t_age.getGlobalBounds().width / 2.f) - this->t_age_enter.getGlobalBounds().width / 2.f - 100,
		this->background_t_age.getPosition().y + (this->background_t_age.getGlobalBounds().height / 2.f) - this->t_age_enter.getGlobalBounds().height / 2.f
	);

	this->t_2_age_enter.setFont(this->font);
	this->t_2_age_enter.setFillColor(sf::Color::Black);
	this->t_2_age_enter.setCharacterSize(24);
	this->t_2_age_enter.setPosition(
		this->background_t_2_age.getPosition().x + (this->background_t_2_age.getGlobalBounds().width / 2.f) - this->t_2_age_enter.getGlobalBounds().width / 2.f - 100,
		this->background_t_2_age.getPosition().y + (this->background_t_2_age.getGlobalBounds().height / 2.f) - this->t_2_age_enter.getGlobalBounds().height / 2.f
	);


	this->t_rounds.setString("Number of rounds do you want to play:");
	this->t_rounds.setFont(this->font);

	this->t_rounds.setFillColor(sf::Color::Black);

	this->t_rounds.setCharacterSize(44);

	this->t_rounds.setPosition(250, 100);

	this->background_rounds.setFillColor(sf::Color::White);

	this->background_rounds.setPosition(900, 75);

	this->background_rounds.setSize(sf::Vector2f(400, 100));

	this->t_rounds_enter.setFont(this->font);
	this->t_rounds_enter.setFillColor(sf::Color::Black);
	this->t_rounds_enter.setCharacterSize(24);
	this->t_rounds_enter.setPosition(
		this->background_rounds.getPosition().x + (this->background_rounds.getGlobalBounds().width / 2.f) - this->t_rounds_enter.getGlobalBounds().width / 2.f - 100,
		this->background_rounds.getPosition().y + (this->background_rounds.getGlobalBounds().height / 2.f) - this->t_rounds_enter.getGlobalBounds().height / 2.f
	);

	this->send = new Button(1400, 800, 250, 75,
		&this->font, "Send",
		sf::Color(70, 70, 70, 200), sf::Color(150, 150, 150, 255), sf::Color(20, 20, 20, 200));
}


void InitState::initBackground()
{
	this->background.setSize(
		sf::Vector2f
		(
			static_cast<float>(this->window->getSize().x),
			static_cast<float>(this->window->getSize().y)
		)
	);
	if (!this->backgroundTexture.loadFromFile("../LostCitiesDuelGraphic/Resources/Images/Background/bg2.png"))
	{
		throw "ERROR:INIT_STATE::FAILED_TO_LOAD_BACKGROUND_TEXTURE";
	}
	this->background.setTexture(&this->backgroundTexture);
}

void InitState::initFonts()
{
	if (!this->font.loadFromFile("../LostCitiesDuelGraphic/Fonts/Dosis-Light.otf"))
	{
		throw("ERROR:Could not load font!");
	}
}
