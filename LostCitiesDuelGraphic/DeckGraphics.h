#pragma once
#include "Entity.h"
#include "..\Lost Cities Duel\Deck.h"

class DeckGraphics :
	public Entity
{
public:
	DeckGraphics(float x, float y,sf::Texture* texture);
	virtual ~DeckGraphics();

	void updateDeck(const float& dt, sf::RenderWindow* window);
	Deck getDeck();
	uint16_t getCardsNumber();
	bool getUploadStat();
	Card getUploadedCard();
	Card getPickCard();
	bool isClicked();
	uint16_t getNumberOfCards();


	void dealCardsG(Player& p1, Player& p2);

	//void initVariables();
public:
	Deck deck;
	Card card;

private:
	//sf::Texture* texture;
	bool card_upload = false;

	/*sf::RectangleShape shape;
	sf::Font* font;
	sf::Text text;*/
};

