#include "FinalState.h"

FinalState::FinalState(sf::RenderWindow* window, std::stack<State*>* states, sf::Text pl1, sf::Text pl2,int score1,int score2) :
	State(window, states)
{
	this->m_player1 = pl1;
	this->m_player2 = pl2;

	this->sc1 = score1;
	this->sc2 = score2;

	this->initBackground();
	this->initFonts();
	this->initText();
}

void FinalState::render(sf::RenderTarget* target)
{
	if (target == nullptr)
		target = this->window;

	target->draw(this->background);

	target->draw(this->m_player1);
	target->draw(this->m_player2);

	target->draw(this->m_score_player1);
	target->draw(this->m_score_player2);

	this->ok->render(target);

	if (this->number_of_rounds == 0)
	{
		target->draw(this->game_over);
		target->draw(this->winner);
		if (this->sc1 > this->sc2) 
		{
			this->the_winnner.setString(this->m_player1.getString());
		}
		else
		{
			this->the_winnner.setString(this->m_player2.getString());
		}
		target->draw(this->the_winnner);
	}
}

void FinalState::update(const float& dt)
{
	this->updateInput(dt);

	this->ok->update(this->mousePosView);

	if (this->ok->isPressed())
	{
		this->states->pop();
		this->states->pop();

		if (this->number_of_rounds == 0)
		{
			this->states->pop();
		}
	}
	
}

void FinalState::updateInput(const float& dt)
{
	this->updateMousePosition();
}

void FinalState::initText()
{
	this->m_player1.setPosition(700, 200);
	this->m_player1.setOutlineColor(sf::Color::Black);

	this->m_player2.setPosition(1100, 200);
	this->m_player2.setOutlineColor(sf::Color::Black);

	this->m_score_player1.setPosition(700, 300);
	this->m_score_player1.setFillColor(sf::Color::White);
	this->m_score_player1.setCharacterSize(80);
	this->m_score_player1.setOutlineColor(sf::Color::Black);
	this->m_score_player1.setOutlineThickness(5);
	this->m_score_player1.setFont(this->font);
	this->m_score_player1.setString(std::to_string(this->sc1));

	this->m_score_player2.setPosition(1100, 300);
	this->m_score_player2.setFillColor(sf::Color::White);
	this->m_score_player2.setCharacterSize(80);
	this->m_score_player2.setOutlineColor(sf::Color::Black);
	this->m_score_player2.setOutlineThickness(5);
	this->m_score_player2.setFont(this->font);
	this->m_score_player2.setString(std::to_string(this->sc2));

	this->game_over.setPosition(800,450 );
	this->game_over.setFillColor(sf::Color::White);
	this->game_over.setCharacterSize(80);
	this->game_over.setOutlineColor(sf::Color::Black);
	this->game_over.setOutlineThickness(5);
	this->game_over.setFont(this->font);
	this->game_over.setString("Game Over!");

	this->winner.setPosition(800, 600);
	this->winner.setFillColor(sf::Color::White);
	this->winner.setCharacterSize(80);
	this->winner.setOutlineColor(sf::Color::Black);
	this->winner.setOutlineThickness(5);
	this->winner.setFont(this->font);
	this->winner.setString("Winner: ");

	this->the_winnner.setPosition(1100, 600);
	this->the_winnner.setFillColor(sf::Color::White);
	this->the_winnner.setCharacterSize(80);
	this->the_winnner.setOutlineColor(sf::Color::Black);
	this->the_winnner.setOutlineThickness(5);
	this->the_winnner.setFont(this->font);

	ok = new Button(1400, 800, 250, 75,
		&this->font, "ok",
		sf::Color(70, 70, 70, 200), sf::Color(150, 150, 150, 255), sf::Color(20, 20, 20, 200));
}

void FinalState::initBackground()
{
	this->background.setSize(
		sf::Vector2f
		(
			static_cast<float>(this->window->getSize().x),
			static_cast<float>(this->window->getSize().y)
		)
	);
	if (!this->backgroundTexture.loadFromFile("../LostCitiesDuelGraphic/Resources/Images/Background/bg2.png"))
	{
		throw "ERROR:INIT_STATE::FAILED_TO_LOAD_BACKGROUND_TEXTURE";
	}
	this->background.setTexture(&this->backgroundTexture);
}

void FinalState::initFonts()
{
	if (!this->font.loadFromFile("../LostCitiesDuelGraphic/Fonts/Dosis-Light.otf"))
	{
		throw("ERROR:Could not load font!");
	}
}
