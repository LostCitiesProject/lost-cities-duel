#include "GameGraphics.h"

#include "..\Lost Cities Duel\LostCitiesDuelGame.h"

#pragma comment(lib, "Lost Cities Duel") 


int main()
{
	GameGraphics game;
	game.run();

	return 0;
}