#pragma once

#include "CardGraphics.h"
#include "TableGraphics.h"

#include"..\Lost Cities Duel\Deck.h"
#include "..\Lost Cities Duel\Table.h"

class State
{
public:
	virtual void update(const float& dt) = 0;
	virtual void render(sf::RenderTarget* target = nullptr) = 0;
	virtual void updateInput(const float& dt) = 0;
	virtual void updateMousePosition();
	virtual void updateKeyTime(const float& dt);

	virtual void updateScore(const Table& table);

	void endState();
	void pauseState();
	void unpauseState();

	const bool& getQuit() const;
	const bool getKeyTime();

public:
	State(sf::RenderWindow* window, std::stack<State*>* states);
	virtual ~State();

protected:
	std::map<std::string, sf::Texture> textures;
	std::stack<State*>* states;
	sf::RenderWindow* window;
	bool quit;
	bool paused;
	float keyTime;
	float keyTimeMax;

	sf::Vector2i mousePosScreen;
	sf::Vector2i mousePosWindow;
	sf::Vector2f mousePosView;


	int name_confirm = 0;

	sf::Text t_enter;
	sf::Text t_2_enter;

	sf::Text t_rounds;
	sf::Text t_rounds_enter;

	sf::Text t_age_enter;
	sf::Text t_2_age_enter;

	int scorePlayer1;
	int scorePlayer2;

	int number_of_rounds;

	bool discardCondition = false;
};

