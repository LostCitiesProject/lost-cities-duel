#pragma once

#include "Entity.h"

#include "..\Lost Cities Duel\Card.h"

class CardGraphics :
	public Entity
{
private:
	//initializer
	void initVariables();
	void initComponents();


	//void update(const float& dt);
	//void render(sf::RenderTarget* target);

public:
	CardGraphics(float x, float y, sf::Texture* texture, bool moveCondition,uint16_t card_type,Card card);
	CardGraphics(float x, float y, sf::Texture* texture, bool moveCondition, sf::Text numar, uint16_t card_type,Card card);

	virtual ~CardGraphics();

	void reset();

	sf::Texture getTexture();
	sf::Vector2f getCoord();
	uint16_t getCardType();
	Card getCard();

private:
	//Variables
	//sf::RectangleShape shape;
	sf::RectangleShape shape;
	sf::Font* font;
	sf::Text text;

	float initX;
	float initY;
	uint16_t m_card_type;
	Card card;

};

