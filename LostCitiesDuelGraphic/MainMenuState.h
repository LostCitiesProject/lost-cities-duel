#pragma once

#include "InitState.h"
#include"SettingsState.h"
#include "Button.h"

class MainMenuState :
	public State
{
public:
	//Functions
	void update(const float& dt);
	void updateInput(const float& dt);
	void updateButtons();
	void renderButtons(sf::RenderTarget* target=nullptr);
	void render(sf::RenderTarget* target = nullptr);

	//Initialize
	void initVariables();
	void initBackground();
	void initFonts();
	void initButtons();

public:
	MainMenuState(sf::RenderWindow* window, std::stack<State*>* states);
	virtual ~MainMenuState();

private:
	//Variables
	sf::RectangleShape background;
	sf::Texture backgroundTexture;
	sf::Font font;

	std::map<std::string, Button*> buttons;

};

