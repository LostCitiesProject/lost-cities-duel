#include "DeckGraphics.h"

DeckGraphics::DeckGraphics(float x, float y,sf::Texture* texture) :
	Entity(false)
{
	this->createSprite(texture);
	this->setPosition(x, y);
	//this->deck.Shuffle();
	//this->initVariables();
}

DeckGraphics::~DeckGraphics()
{

}

void DeckGraphics::updateDeck(const float& dt, sf::RenderWindow* window)
{

	this->updateMousePosition(window);
	if (this->sprite->getGlobalBounds().contains(this->mousePosView) && sf::Mouse::isButtonPressed(sf::Mouse::Left) && this->card_upload==false)
	{
		this->card=std::move(deck.PickCard());
		this->card_upload = true;
		std::cout << "Update deck!!!";
		//activePlayer.get().AddToHand(card);
	}
}

Deck DeckGraphics::getDeck()
{
	return this->deck;
}

uint16_t DeckGraphics::getCardsNumber()
{
	return this->deck.GetNumberOfCards();
}

bool DeckGraphics::getUploadStat()
{
	if (this->card_upload == false)
		return false;

		return true;
}

Card DeckGraphics::getUploadedCard()
{
	this->card_upload = false;
	return this->card;

}

Card DeckGraphics::getPickCard()
{
	if(card_upload==false)
	this->card = std::move(deck.PickCard());
	this->card_upload = false;
	return card;
}

bool DeckGraphics::isClicked()
{
	if (this->sprite->getGlobalBounds().contains(this->mousePosView) && sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		return true;
	}
	return false;
}

uint16_t DeckGraphics::getNumberOfCards()
{
	return this->deck.GetNumberOfCards();
}



void DeckGraphics::dealCardsG(Player& p1, Player& p2)
{
	this->deck.DealCards(p1, p2,this->deck);
}

//void DeckGraphics::initVariables()
//{
//	this->shape.setPosition(this->sprite->getPosition().x, this->sprite->getPosition().y);
//	this->shape.setSize(sf::Vector2f(50, 50));
//	std::string text("Text");
//	this->font = font;
//	this->text.setFont(*this->font);
//	this->text.setString(text);
//	this->text.setFillColor(sf::Color::White);
//	this->text.setCharacterSize(24);
//	this->text.setPosition(
//		this->shape.getPosition().x + (this->shape.getGlobalBounds().width / 2.f) - this->text.getGlobalBounds().width / 2.f,
//		this->shape.getPosition().y + (this->shape.getGlobalBounds().height / 2.f) - this->text.getGlobalBounds().height / 2.f
//	);
//
//	this->shape.setFillColor(sf::Color::White);
//	//sf::RenderTarget* target;
//	//target=this->w
//	//target->draw(shape);
//}
