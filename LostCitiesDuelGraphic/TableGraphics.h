#pragma once

#include "Entity.h"

class TableGraphics :
	public Entity
{
public:
	TableGraphics(float x, float y, sf::Texture* texture, bool moveCondition);
	virtual ~TableGraphics();
};

