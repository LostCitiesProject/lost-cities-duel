#pragma once

#include "State.h"
#include "GameState.h"


class InitState :
	public State
{
public:
	InitState(sf::RenderWindow* window, std::stack<State*>* states);
	virtual ~InitState();

	void update(const float& dt);
	void updateText(const float& dt);
	void updateInput(const float& dt);

	void render(sf::RenderTarget* target = nullptr);

	void initNameWindow();
	void initBackground();
	void initFonts();

private:
	sf::RectangleShape background;

	sf::RectangleShape background_t;
	sf::RectangleShape background_t_2;

	sf::RectangleShape background_t_age;
	sf::RectangleShape background_t_2_age;

	sf::RectangleShape background_rounds;

	sf::Texture backgroundTexture;
	sf::Font font;


	bool yes;

	std::string playerOne;
	std::string playerTwo;


	sf::Text t;
	sf::Text t_2;

	sf::Text t_age;
	sf::Text t_2_age;

	int select = -1;
	int count = 0;

	std::vector <int> count_k;
	std::vector <int> count_k_age;

	std::vector <int> count_k_rounds;

	Button* send;


};

