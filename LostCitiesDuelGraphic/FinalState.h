#pragma once

#include "State.h"
#include "Button.h"

class FinalState :
	public State
{
public:
	FinalState(sf::RenderWindow* window, std::stack<State*>* states, sf::Text pl1, sf::Text pl2,int score1, int score2);

	void render(sf::RenderTarget* target = nullptr);

	void update(const float& dt);
	void updateInput(const float& dt);

	void initText();
	void initBackground();
	void initFonts();

private:
	sf::RectangleShape background;

	sf::Texture backgroundTexture;
	sf::Font font;

	sf::Text m_player1;
	sf::Text m_player2;

	sf::Text m_score_player1;
	sf::Text m_score_player2;

	sf::Text game_over;
	sf::Text  winner;

	sf::Text the_winnner;

	int sc1;
	int sc2;

	Button * ok;
};

